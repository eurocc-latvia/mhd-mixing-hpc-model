#!/bin/bash
SOLVER_OF=mhdBuoyantBoussinesqPimple
#SLURM_NTASKS=16

## Solver settings
MESH = 1 #0-Very coarse, 1-Coarse(5min compilation), 2-Moderate, 3-Fine, 4-Very fine
CASEFILE = case-transient#case-harmonic#
TRANSPORT = Newtonian#tempdeppowerLaw#
TURBULENCE = kOmegaSST#check if laminar flow chosen in turbulenceProperties!!!
YPLUS = 3300.0#33.0#
MAX_TIME_OF = 300
MAX_TIME_EL = 1000000
TIMESTEP_OF = 0.001
TIMESTEP_EL = 0.001
WRITE = 1
FRAMERATE = 10

## Properties and conditions
DENSITY = 2600
VISCOSITY = 3.75e-7
CONDUCTIVITY = 3.46e6
TH_CONDUCTIVITY = 187.7
HEAT_CAPACITY = 1000
TH_EXPANSION = 4.0e-5
PRANDTL = 0.05
TEMPERATURE = 1000
MELTING_TEMP = 1000
T_AMPLITUDE = 50
MAGNETISATION = 1.4
FREQUENCY = 1

#Furnace Geometry
FURNACE = trapeze4#cylinder#trapeze#box#
#Furnace dimensions
FURNACE_X = 4.000
FURNACE_Y = 1.246
FURNACE_Z = 0.570
FURNACE_X1 = 0.3
FURNACE_X2 = 0.3
FURNACE_Y1 = 0.987
FURNACE_Y2 = 0.860
FURNACE_Z1 = 0.063
#FURNACE_Z2 = 0.2
#Magnet Geometry
MAGNET_RAD = 0.239
MAGNET_HEI = 1.000
#Magnet position
MAGNET_X = 0
MAGNET_Y = 0
MAGNET_Z = -1.040
## Magnet rotation
MAGNET_ANGLE_Y = 0
MAGNET_ANGLE_X = -90

all: clean build run #post

clean:
	@echo 'Cleaning up...'
	@. $(WM_PROJECT_DIR)/bin/tools/CleanFunctions;	cleanCase;	cleanSamples
	@rm -rf 0
	@rm -rf results
	@rm -rf meshElmer
	@rm -f case.sif
	@rm -f furnace.geo
	@rm -f *.avi
	@rm -f *.csv
	@rm -f *.gif
	@rm -f log
	@rm -f O2E*
	@rm -f *.png
	@rm -f *.unv
	@rm -f *.vtu

build:
	@echo 'Building...'
	cp $(CASEFILE).sif case.sif
	cp $(FURNACE).geo furnace.geo
	
	sed -i 's/x0 = [^/]*/x0 = $(FURNACE_X)/' furnace.geo
	sed -i 's/y0 = [^/]*/y0 = $(FURNACE_Y)/' furnace.geo
	sed -i 's/z0 = [^/]*/z0 = $(FURNACE_Z)/' furnace.geo
	sed -i 's/x1 = [^;]*/x1 = $(FURNACE_X1)/' furnace.geo
	sed -i 's/x2 = [^;]*/x2 = $(FURNACE_X2)/' furnace.geo
	sed -i 's/y1 = [^;]*/y1 = $(FURNACE_Y1)/' furnace.geo
	sed -i 's/y2 = [^;]*/y2 = $(FURNACE_Y2)/' furnace.geo
	sed -i 's/z1 = [^;]*/z1 = $(FURNACE_Z1)/' furnace.geo
	#sed -i 's/z2 = [^;]*/z2 = $(FURNACE_Z2)/' furnace.geo
	sed -i 's/globalMeshFineness = [^;]*/globalMeshFineness = $(MESH)/' furnace.geo
	sed -i 's/frequency = [^;]*/frequency = $(FREQUENCY)/' furnace.geo
	sed -i 's/magnet_rad = [^;]*/magnet_rad = $(MAGNET_RAD)/' furnace.geo
	sed -i 's/magnet_x = [^;]*/magnet_x = $(MAGNET_X)/' furnace.geo
	sed -i 's/magnet_y = [^;]*/magnet_y = $(MAGNET_Y)/' furnace.geo
	sed -i 's/magnet_z = [^;]*/magnet_z = $(MAGNET_Z)/' furnace.geo
	sed -i 's/nu = [^;]*/nu = $(VISCOSITY)/' furnace.geo
	sed -i 's/yplus = [^;]*/yplus = $(YPLUS)/' furnace.geo
	sed -i 's/highT [^;]*/highT $(shell expr $(TEMPERATURE) + $(T_AMPLITUDE))/' 0_orig/T
	sed -i 's/lowT [^;]*/lowT $(shell expr $(TEMPERATURE) - $(T_AMPLITUDE))/' 0_orig/T
	sed -i 's/highT [^;]*/highT $(shell expr $(TEMPERATURE) + $(T_AMPLITUDE))/' system/setFieldsDict
	sed -i 's/lowT [^;]*/lowT $(shell expr $(TEMPERATURE) - $(T_AMPLITUDE))/' system/setFieldsDict
	sed -i 's/box [^;]*/box (-$(FURNACE_X) -$(FURNACE_Y) 0) ($(FURNACE_X) $(FURNACE_Y) $(FURNACE_Z))/' system/setFieldsDict
	sed -i 's/funaceGeometry = "[^"]*/funaceGeometry = "$(FURNACE)/' geomesh*
	sed -i 's/furnaceDimX = [^\n]*/furnaceDimX = $(FURNACE_X)/' geomesh*
	sed -i 's/furnaceDimY = [^\n]*/furnaceDimY = $(FURNACE_Y)/' geomesh*
	sed -i 's/furnaceDimZ = [^\n]*/furnaceDimZ = $(FURNACE_Z)/' geomesh*
	sed -i 's/furnaceDimX1 = [^\n]*/furnaceDimX1 = $(FURNACE_X1)/' geomesh*
	sed -i 's/furnaceDimX2 = [^\n]*/furnaceDimX2 = $(FURNACE_X2)/' geomesh*
	sed -i 's/furnaceDimY1 = [^\n]*/furnaceDimY1 = $(FURNACE_Y1)/' geomesh*
	sed -i 's/furnaceDimY2 = [^\n]*/furnaceDimY2 = $(FURNACE_Y2)/' geomesh*
	sed -i 's/furnaceDimZ1 = [^\n]*/furnaceDimZ1 = $(FURNACE_Z1)/' geomesh*
	#sed -i 's/furnaceDimZ2 = [^\n]*/furnaceDimZ2 = $(FURNACE_Z2)/' geomesh*
	sed -i 's/magnetDimR = [^\n]*/magnetDimR = $(MAGNET_RAD)/' geomesh*
	sed -i 's/magnetDimH = [^\n]*/magnetDimH = $(MAGNET_HEI)/' geomesh*
	sed -i 's/magnetOffsetX = [^\n]*/magnetOffsetX = $(MAGNET_X)/' geomesh*
	sed -i 's/magnetOffsetY = [^\n]*/magnetOffsetY = $(MAGNET_Y)/' geomesh*
	sed -i 's/magnetOffsetZ = [^\n]*/magnetOffsetZ = $(MAGNET_Z)/' geomesh*
	sed -i 's/magnetAngle1 = [^\n]*/magnetAngle1 = $(MAGNET_ANGLE_Y)/' geomesh*
	sed -i 's/magnetAngle2 = [^\n]*/magnetAngle2 = $(MAGNET_ANGLE_X)/' geomesh*
	sed -i 's/globalMeshFineness = [^\n]*/globalMeshFineness = $(MESH)/' geomesh*
	sed -i 's/furnaceDimX = [^\n]*/furnaceDimX = $(FURNACE_X)/' showOFMesh.py
	sed -i 's/furnaceDimY = [^\n]*/furnaceDimY = $(FURNACE_Y)/' showOFMesh.py
	sed -i 's/furnaceDimZ = [^\n]*/furnaceDimZ = $(FURNACE_Z)/' showOFMesh.py
	sed -i 's/furnaceDimX = [^\n]*/furnaceDimX = $(FURNACE_X)/' showMesh.py
	sed -i 's/furnaceDimY = [^\n]*/furnaceDimY = $(FURNACE_Y)/' showMesh.py
	sed -i 's/furnaceDimZ = [^\n]*/furnaceDimZ = $(FURNACE_Z)/' showMesh.py
	sed -i 's/magnetPosX = [^;]*/magnetPosX = $(MAGNET_X)/' showMesh.py
	sed -i 's/magnetPosY = [^;]*/magnetPosY = $(MAGNET_Y)/' showMesh.py
	sed -i 's/magnetPosZ = [^;]*/magnetPosZ = $(MAGNET_Z)/' showMesh.py
	sed -i 's/w = [^*]*/w = $(FREQUENCY)/' case.sif
	sed -i 's/br = [^/]*/br = $(MAGNETISATION)/' case.sif
	sed -i 's/sigma = [^\n]*/sigma = $(CONDUCTIVITY)/' case.sif
	sed -i 's/deltat = [^\n]*/deltat = $(TIMESTEP_EL)/' case.sif
	sed -i 's/angle1 = [^*]*/angle1 = $(MAGNET_ANGLE_Y)/' case.sif
	sed -i 's/angle2 = [^*]*/angle2 = $(MAGNET_ANGLE_X)/' case.sif
	sed -i 's/Steady State Min Iterations = [^!]*/Steady State Min Iterations = $(MAX_TIME_EL)/' case.sif 
	sed -i 's/Steady State Max Iterations = [^!]*/Steady State Max Iterations = $(MAX_TIME_EL)/' case.sif 
	sed -i 's/Timestep Intervals = [^!]*/Timestep Intervals = $(MAX_TIME_EL)/' case.sif 
	sed -i 's/transportModel [^;]*/transportModel $(TRANSPORT)/' constant/transportProperties
	sed -i 's/rho \[1 -3 0 0 0 0 0\] [^;]*/rho \[1 -3 0 0 0 0 0\] $(DENSITY)/' constant/transportProperties
	sed -i 's/nu \[0 2 -1 0 0 0 0\] [^;]*/nu \[0 2 -1 0 0 0 0\] $(VISCOSITY)/' constant/transportProperties
	sed -i 's/Tamp \[0 0 0 1 0 0 0\] [^;]*/Tamp \[0 0 0 1 0 0 0\] $(T_AMPLITUDE)/' constant/transportProperties
	sed -i 's/Tbase \[0 0 0 1 0 0 0\] [^;]*/Tbase \[0 0 0 1 0 0 0\] $(MELTING_TEMP)/' constant/transportProperties
	sed -i 's/TRef \[0 0 0 1 0 0 0\] [^;]*/TRef \[0 0 0 1 0 0 0\] $(TEMPERATURE)/' constant/transportProperties
	sed -i 's/k_thermal \[1 1 -3 -1 0 0 0\] [^;]*/k_thermal \[1 1 -3 -1 0 0 0\] $(TH_CONDUCTIVITY)/' constant/transportProperties
	sed -i 's/beta \[0 0 0 -1 0 0 0\] [^;]*/beta \[0 0 0 -1 0 0 0\] $(TH_EXPANSION)/' constant/transportProperties
	sed -i 's/Cp \[0 2 -2 -1 0 0 0\] [^;]*/Cp \[0 2 -2 -1 0 0 0\] $(HEAT_CAPACITY)/' constant/transportProperties
	sed -i 's/Pr \[0 0 0 0 0 0 0\] [^;]*/Pr \[0 0 0 0 0 0 0\] $(PRANDTL)/' constant/transportProperties
	sed -i 's/RASModel [^;]*/RASModel $(TURBULENCE)/' constant/turbulenceProperties
	sed -i 's/numberOfSubdomains [^;]*/numberOfSubdomains $(shell expr $(SLURM_NTASKS) \/ 2)/' system/decomposeParDict
	sed -i 's/application [^;]*/application $(SOLVER_OF)/' system/controlDict
	sed -i 's/deltaT [^;]*/deltaT $(TIMESTEP_OF)/' system/controlDict
	sed -i 's/endTime [^;]*/endTime $(MAX_TIME_OF)/' system/controlDict
	sed -i 's/writeInterval [^;]*/writeInterval $(WRITE)/' system/controlDict
	sed -i 's/title = "[^"]*/title = "$(SLURM_JOB_NAME)/' post-temp.py
	sed -i 's/frequency = [^\n]*/frequency = $(FREQUENCY)/' post-temp.py
	sed -i 's/temperature = [^\n]*/temperature = $(TEMPERATURE)/' post-temp.py
	sed -i 's/furnaceDimX = [^\n]*/furnaceDimX = $(FURNACE_X)/' post-temp.py
	sed -i 's/furnaceDimY = [^\n]*/furnaceDimY = $(FURNACE_Y)/' post-temp.py
	sed -i 's/furnaceDimZ = [^\n]*/furnaceDimZ = $(FURNACE_Z)/' post-temp.py
	sed -i 's/furnaceDimX = [^\n]*/furnaceDimX = $(FURNACE_X)/' post.py
	sed -i 's/f=[^Hz]*/f=$(FREQUENCY)/' post.py

	cp -r 0_orig 0
	gmsh -3 furnace.geo -o furnace.msh -format msh2
	gmshToFoam furnace.msh
	ElmerGrid 14 5 furnace.msh
	@rm -f furnace.msh
	changeDictionary
	setFields #initializes temperature distribution
	salome -t ./geomeshtry.py
	salome -t ./geomeshow.py
	ElmerGrid 2 5 meshElmer
	decomposePar -force -latestTime #decompose OpenFOAM
	checkMesh
	ElmerGrid 2 2 meshElmer -metis $(shell expr $(SLURM_NTASKS) / 2) #decomposes Elmer
	pvpython showOFMesh.py
	pvpython showMesh.py

run:
	@echo 'Running...' #in case of hypert-hreading use option: --use-hwthread-cpus
	mpirun -n $(shell expr $(SLURM_NTASKS) / 2) $(SOLVER_OF) -parallel : -n $(shell expr $(SLURM_NTASKS) / 2) ElmerSolver_mpi case.sif #> log &
	
post:
	@echo 'Postprocessing...'
	@rm -f O2E*
	reconstructPar -newTimes -noZero
	pvpython post.py
	pvpython post-temp.py
#	$(SOLVER_OF) -postProcess -noZero -func yPlus > results
