#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.3.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
## Path to Salome directory

## Geometry parameters
funaceGeometry = "trapeze4"
## Furnace dimensions
furnaceDimX = 4.000
furnaceDimY = 1.246
furnaceDimZ = 0.570
furnaceDimX1 = 0.3
furnaceDimX2 = 0.3
furnaceDimY1 = 0.987
furnaceDimY2 = 0.860
furnaceDimZ1 = 0.063
furnaceDimZ2 = (3*furnaceDimZ1+furnaceDimZ/2)/2
## Magnet dimensions
magnetDimR = 0.239
magnetDimH = 1.000
## Magnet translation
magnetOffsetX = 0
magnetOffsetY = 0
magnetOffsetZ = -1.040
## Magnet rotation around Y and X axis
magnetAngle1 = 0
magnetAngle2 = -90
## Air domain dimensions
airDimXYZ = 8*max(furnaceDimX,furnaceDimY,furnaceDimZ)

## Mesh parameters
## Set Fineness for mesh # Very coarse 0 # Coarse 1 # Moderate 2 # Fine 3 # Very fine 4 # Custom 5
globalMeshFineness = 1 
maxGeomSize = airDimXYZ/2
minGeomSize = min(furnaceDimX,furnaceDimY,furnaceDimZ)/5/(globalMeshFineness+0.01)/5
furnaceGeomSize = min(furnaceDimX,furnaceDimY,furnaceDimZ)/2/(globalMeshFineness+0.01)/2
magnetGeomSize = min(magnetDimR,magnetDimH)/2/(max(globalMeshFineness,0.5)+0.01)

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

geompy = geomBuilder.New()
## Center point
O = geompy.MakeVertex(0, 0, 0)
## Axis
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

if funaceGeometry == "cylinder":
    # Make ___CYLINDER___ furnace and center
    myFluidContainer = geompy.MakeCylinderRH((furnaceDimX+furnaceDimY)/4, furnaceDimZ)
    Multi_Rotation_1 = geompy.MultiRotate1DNbTimes(myFluidContainer, OZ, 4)
    Multi_Rotation_2 = geompy.Rotate(Multi_Rotation_1, OZ, 315*math.pi/180.0)
    myFluidContainer = geompy.MakePartition([Multi_Rotation_2], [], [], [], geompy.ShapeType["SOLID"], 0, [], 0)
    geompy.TranslateDXDYDZ(myFluidContainer, 0, 0, -0.5*furnaceDimZ)
elif funaceGeometry == "box":
    # Make ___BOX___ furnace and center
    myFluidContainer = geompy.MakeBoxDXDYDZ(furnaceDimX, furnaceDimY, furnaceDimZ)
    geompy.TranslateDXDYDZ(myFluidContainer, -0.5*furnaceDimX, -0.5*furnaceDimY, -0.5*furnaceDimZ)
elif funaceGeometry == "trapeze":
    # Make ___TRAPEZE___ furnace and center
    sk = geompy.Sketcher3D()
    sk.addPointsAbsolute(0, 0, 0)
    sk.addPointsAbsolute(0, furnaceDimY, 0)
    sk.addPointsAbsolute(0, furnaceDimY1 + furnaceDimY, furnaceDimZ)
    sk.addPointsAbsolute(0, furnaceDimY2, furnaceDimZ)
    sk.addPointsAbsolute(0, 0, 0)
    a3D_Sketcher_1 = sk.wire()
    Face_1 = geompy.MakeFaceWires([a3D_Sketcher_1], 1)
    myFluidContainer = geompy.MakePrismVecH(Face_1, OX, furnaceDimX)
    geompy.TranslateDXDYDZ(myFluidContainer, -0.5*furnaceDimX, -0.5*furnaceDimY, -0.5*furnaceDimZ)
elif funaceGeometry == "trapeze4":
    # Make ___TRAPEZE___ furnace and center
    Point101 = geompy.MakeVertex(-furnaceDimX/2-furnaceDimX1/3,-furnaceDimY/2-furnaceDimY1, furnaceDimZ/2)
    Point102 = geompy.MakeVertex(-furnaceDimX/2-furnaceDimX1, -furnaceDimY/2, furnaceDimZ/2)
    Point103 = geompy.MakeVertex(-furnaceDimX/2-furnaceDimX1,	 furnaceDimY/2, furnaceDimZ/2)
    Point104 = geompy.MakeVertex(-furnaceDimX/2-furnaceDimX1/3, furnaceDimY/2+furnaceDimY2, furnaceDimZ/2)
    Point105 = geompy.MakeVertex(-furnaceDimX/2-furnaceDimX1/3,-furnaceDimY/2-furnaceDimY1, furnaceDimZ2)
    Point106 = geompy.MakeVertex(-furnaceDimX/2-furnaceDimX1,	-furnaceDimY/2, furnaceDimZ2)
    Point107 = geompy.MakeVertex(-furnaceDimX/2-furnaceDimX1,	 furnaceDimY/2, furnaceDimZ2)
    Point108 = geompy.MakeVertex(-furnaceDimX/2-furnaceDimX1/3, furnaceDimY/2+furnaceDimY2, furnaceDimZ2)

    Point1005 = geompy.MakeVertex(-furnaceDimX/2,-furnaceDimY/2-furnaceDimY1, furnaceDimZ1)
    Point1006 = geompy.MakeVertex(-furnaceDimX/2,	  -furnaceDimY/2,-furnaceDimZ/2)
    Point1007 = geompy.MakeVertex(-furnaceDimX/2,	   furnaceDimY/2,-furnaceDimZ/2)
    Point1008 = geompy.MakeVertex(-furnaceDimX/2, furnaceDimY/2+furnaceDimY2, furnaceDimZ1)

    Point10005 = geompy.MakeVertex(furnaceDimX/2,-furnaceDimY/2-furnaceDimY1, furnaceDimZ1)
    Point10006 = geompy.MakeVertex(furnaceDimX/2,	  -furnaceDimY/2,-furnaceDimZ/2)
    Point10007 = geompy.MakeVertex(furnaceDimX/2,	   furnaceDimY/2,-furnaceDimZ/2)
    Point10008 = geompy.MakeVertex(furnaceDimX/2, furnaceDimY/2+furnaceDimY2, furnaceDimZ1)

    Point100001 = geompy.MakeVertex(furnaceDimX/2+furnaceDimX2/3,-furnaceDimY/2-furnaceDimY1, furnaceDimZ/2)
    Point100002 = geompy.MakeVertex(furnaceDimX/2+furnaceDimX2,	  -furnaceDimY/2, furnaceDimZ/2)
    Point100003 = geompy.MakeVertex(furnaceDimX/2+furnaceDimX2,	   furnaceDimY/2, furnaceDimZ/2)
    Point100004 = geompy.MakeVertex(furnaceDimX/2+furnaceDimX2/3, furnaceDimY/2+furnaceDimY2, furnaceDimZ/2)
    Point100005 = geompy.MakeVertex(furnaceDimX/2+furnaceDimX2/3,-furnaceDimY/2-furnaceDimY1, furnaceDimZ2)
    Point100006 = geompy.MakeVertex(furnaceDimX/2+furnaceDimX2,	  -furnaceDimY/2, furnaceDimZ2)
    Point100007 = geompy.MakeVertex(furnaceDimX/2+furnaceDimX2,	   furnaceDimY/2, furnaceDimZ2)
    Point100008 = geompy.MakeVertex(furnaceDimX/2+furnaceDimX2/3, furnaceDimY/2+furnaceDimY2, furnaceDimZ2)

    Line201 = geompy.MakeVector(Point101,Point102)
    Line202 = geompy.MakeVector(Point102,Point103)
    Line203 = geompy.MakeVector(Point103,Point104)
    Line204 = geompy.MakeVector(Point104,Point108)
    Line205 = geompy.MakeVector(Point108,Point107)
    Line206 = geompy.MakeVector(Point107,Point106)
    Line207 = geompy.MakeVector(Point106,Point105)
    Line208 = geompy.MakeVector(Point105,Point101)
    Line2005 = geompy.MakeVector(Point1008,Point1007)
    Line2006 = geompy.MakeVector(Point1007,Point1006)
    Line2007 = geompy.MakeVector(Point1006,Point1005)
    Line20005 = geompy.MakeVector(Point10008,Point10007)
    Line20006 = geompy.MakeVector(Point10007,Point10006)
    Line20007 = geompy.MakeVector(Point10006,Point10005)
    Line200001 = geompy.MakeVector(Point100001,Point100002)
    Line200002 = geompy.MakeVector(Point100002,Point100003)
    Line200003 = geompy.MakeVector(Point100003,Point100004)
    Line200004 = geompy.MakeVector(Point100004,Point100008)
    Line200005 = geompy.MakeVector(Point100008,Point100007)
    Line200006 = geompy.MakeVector(Point100007,Point100006)
    Line200007 = geompy.MakeVector(Point100006,Point100005)
    Line200008 = geompy.MakeVector(Point100005,Point100001)

    Line301 = geompy.MakeVector(Point101,Point100001)
    Line310 = geompy.MakeVector(Point104,Point100004)
    Line313 = geompy.MakeVector(Point105,Point1005)
    Line314 = geompy.MakeVector(Point1005,Point10005)
    Line315 = geompy.MakeVector(Point10005,Point100005)
    Line316 = geompy.MakeVector(Point106,Point1006)
    Line317 = geompy.MakeVector(Point1006,Point10006)
    Line318 = geompy.MakeVector(Point10006,Point100006)
    Line319 = geompy.MakeVector(Point107,Point1007)
    Line320 = geompy.MakeVector(Point1007,Point10007)
    Line321 = geompy.MakeVector(Point10007,Point100007)
    Line322 = geompy.MakeVector(Point108,Point1008)
    Line323 = geompy.MakeVector(Point1008,Point10008)
    Line324 = geompy.MakeVector(Point10008,Point100008)
    Line3001 = geompy.MakeVector(Point102,Point106)
    Line3002 = geompy.MakeVector(Point103,Point107)
    Line3007 = geompy.MakeVector(Point100002,Point100006)
    Line3008 = geompy.MakeVector(Point100003,Point100007)

    Wire401 = geompy.MakeWire([Line201,Line3001,Line207,Line208])
    Wire402 = geompy.MakeWire([Line202,Line3002,Line206,Line3001])
    Wire403 = geompy.MakeWire([Line203,Line204,Line205,Line3002])
    Wire400001 = geompy.MakeWire([Line200001,Line3007,Line200007,Line200008])
    Wire400002 = geompy.MakeWire([Line200002,Line3008,Line200006,Line3007])
    Wire400003 = geompy.MakeWire([Line200003,Line200004,Line200005,Line3008])

    Wire501 = geompy.MakeWire([Line301,Line208,Line313,Line314,Line315,Line200008])
    Wire500001 = geompy.MakeWire([Line310,Line204,Line322,Line323,Line324,Line200004])

    Wire6001 = geompy.MakeWire([Line313,Line207,Line316,Line2007])
    Wire6002 = geompy.MakeWire([Line314,Line2007,Line317,Line20007])
    Wire6003 = geompy.MakeWire([Line315,Line20007,Line318,Line200007])
    Wire6004 = geompy.MakeWire([Line316,Line206,Line319,Line2006])
    Wire6005 = geompy.MakeWire([Line317,Line2006,Line320,Line20006])
    Wire6006 = geompy.MakeWire([Line318,Line20006,Line321,Line200006])
    Wire6007 = geompy.MakeWire([Line319,Line205,Line322,Line2005])
    Wire6008 = geompy.MakeWire([Line320,Line2005,Line323,Line20005])
    Wire6009 = geompy.MakeWire([Line321,Line20005,Line324,Line200005])
    Wire6010 = geompy.MakeWire([Line201,Line202,Line203,Line310,Line200003,Line200002,Line200001,Line301])

    Surface701 = geompy.MakeFace(Wire401,1)
    Surface702 = geompy.MakeFace(Wire402,1)
    Surface703 = geompy.MakeFace(Wire403,1)
    Surface710 = geompy.MakeFace(Wire400001,1)
    Surface711 = geompy.MakeFace(Wire400002,1)
    Surface712 = geompy.MakeFace(Wire400003,1)
    Surface7001 = geompy.MakeFace(Wire501,1)
    Surface7010 = geompy.MakeFace(Wire500001,1)
    Surface700001 = geompy.MakeFace(Wire6001,1)
    Surface700002 = geompy.MakeFace(Wire6002,1)
    Surface700003 = geompy.MakeFace(Wire6003,1)
    Surface700004 = geompy.MakeFace(Wire6004,1)
    Surface700005 = geompy.MakeFace(Wire6005,1)
    Surface700006 = geompy.MakeFace(Wire6006,1)
    Surface700007 = geompy.MakeFace(Wire6007,1)
    Surface700008 = geompy.MakeFace(Wire6008,1)
    Surface700009 = geompy.MakeFace(Wire6009,1)
    Surface700010 = geompy.MakeFace(Wire6010,1)

    faces = []
    faces.append(Surface701)
    faces.append(Surface702)
    faces.append(Surface703)
    faces.append(Surface710)
    faces.append(Surface711)
    faces.append(Surface712)
    faces.append(Surface7001)
    faces.append(Surface7010)
    faces.append(Surface700001)
    faces.append(Surface700002)
    faces.append(Surface700003)
    faces.append(Surface700004)
    faces.append(Surface700005)
    faces.append(Surface700006)
    faces.append(Surface700007)
    faces.append(Surface700008)
    faces.append(Surface700009)
    faces.append(Surface700010)

    shells = []
    shell = geompy.MakeSewing(faces,1.e-6)
    shells.append(shell)
    faces = geompy.SubShapeAllSorted(shell, geompy.ShapeType["FACE"])
    myFluidContainer = geompy.MakeSolid(shells)

## Make air domain and center
myAirBox = geompy.MakeBoxDXDYDZ(airDimXYZ, airDimXYZ, airDimXYZ)
geompy.TranslateDXDYDZ(myAirBox, -0.5*airDimXYZ, -0.5*airDimXYZ, -0.5*airDimXYZ)
## Default cylinder axes in Z direction
myPermanentMagnet = geompy.MakeCylinderRH(magnetDimR, magnetDimH)
geompy.TranslateDXDYDZ(myPermanentMagnet, magnetOffsetX, magnetOffsetY, magnetOffsetZ-0.5*magnetDimH)
## Vector for rotation around local Y axes
rotationVector1 = geompy.MakeTranslation(OY, magnetOffsetX, magnetOffsetY, magnetOffsetZ)
## Vector for rotation around local X axes
rotationVector2 = geompy.MakeTranslation(OX, magnetOffsetX, magnetOffsetY, magnetOffsetZ)
## 1st rotation around local Y axes
geompy.Rotate(myPermanentMagnet, rotationVector1, magnetAngle1*math.pi/180.0)
geompy.Rotate(rotationVector2, rotationVector1, magnetAngle1*math.pi/180.0)
## 2nd rotation around local X axes
geompy.Rotate(myPermanentMagnet, rotationVector2, magnetAngle2*math.pi/180.0)
## Combine all geometry in a partition
combinedGeometry = geompy.MakePartition([myFluidContainer, myPermanentMagnet, myAirBox], [], [], [], geompy.ShapeType["SOLID"], 0, [], 0)
## Define required domains for Elmer
## Define funace domain
furnace = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["SOLID"])
geompy.UnionIDs(furnace, [2])
## Define magnet domain
magnet = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["SOLID"])
geompy.UnionIDs(magnet, [36])
## Define air domain
air = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["SOLID"])
geompy.UnionIDs(air, [49])
## Define outer surface
outer = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
geompy.UnionIDs(outer, [68, 73, 81, 51, 78, 61])
## Define additional domains for meshing
if funaceGeometry == "cylinder":
    furnacePlusX = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnacePlusX, [14])#14-box & cylinder, 34-trapezoid
    furnaceMinusX = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusX, [28])#4-box #28-cylinder, 32-trapezoid
    funacePlusY = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(funacePlusY, [21])#28-box #21-cylinder, 14-trapezoid
    furnaceMinusY = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusY, [4])#24-box #4-cylinder, 28-trapezoid
    furnacePlusZ = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnacePlusZ, [32])#34-box #32-cylinder, 21-trapezoid
    furnaceMinusZ = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusZ, [34])#32-box #34-cylinder, 4-trapezoid
elif funaceGeometry == "box":
    furnacePlusX = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnacePlusX, [14])
    furnaceMinusX = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusX, [4])
    funacePlusY = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(funacePlusY, [28])
    furnaceMinusY = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusY, [24])
    furnacePlusZ = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnacePlusZ, [34])
    furnaceMinusZ = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusZ, [32])
elif funaceGeometry == "trapeze":
    furnacePlusX = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnacePlusX, [34])
    furnaceMinusX = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusX, [32])
    funacePlusY = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(funacePlusY, [14])
    furnaceMinusY = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusY, [28])
    furnacePlusZ = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnacePlusZ, [21])
    furnaceMinusZ = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusZ, [4])
elif funaceGeometry == "trapeze4":
    furnace = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["SOLID"])
    geompy.UnionIDs(furnace, [2])
    ## Define magnet domain
    magnet = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["SOLID"])
    geompy.UnionIDs(magnet, [104])
    ## Define air domain
    air = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["SOLID"])
    geompy.UnionIDs(air, [117])
    ## Define outer surface
    outer = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(outer, [119,129,136,141,146,149])
    furnacePlusX = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnacePlusX, [39,46,53,78,91,100])
    furnaceMinusX = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusX, [4,14,21,68,81,94])
    funacePlusY = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(funacePlusY, [60,97])
    furnaceMinusY = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusY, [28,73])
    furnacePlusZ = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnacePlusZ, [102])
    furnaceMinusZ = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
    geompy.UnionIDs(furnaceMinusZ, [86])
# ## Define furnace surface in plusX direction
# furnacePlusX = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
# geompy.UnionIDs(furnacePlusX, [14])#14-box & cylinder, 34-trapezoid
# ## Define furnace surface in minusX direction
# furnaceMinusX = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
# geompy.UnionIDs(furnaceMinusX, [28])#4-box #28-cylinder, 32-trapezoid
# ## Define furnace surface in plusY direction
# funacePlusY = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
# geompy.UnionIDs(funacePlusY, [21])#28-box #21-cylinder, 14-trapezoid
# ## Define furnace surface in minusY direction
# furnaceMinusY = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
# geompy.UnionIDs(furnaceMinusY, [4])#24-box #4-cylinder, 28-trapezoid
# ## Define furnace surface in plusZ direction
# furnacePlusZ = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
# geompy.UnionIDs(furnacePlusZ, [32])#34-box #32-cylinder, 21-trapezoid
# ## Define furnace surface in minusZ direction
# furnaceMinusZ = geompy.CreateGroup(combinedGeometry, geompy.ShapeType["FACE"])
# geompy.UnionIDs(furnaceMinusZ, [34])#32-box #34-cylinder, 4-trapezoid

## Geometry that will appear in the study
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( myFluidContainer, 'myFluidContainer' )
geompy.addToStudy( rotationVector1, 'rotationVector1' )
geompy.addToStudy( rotationVector2, 'rotationVector2' )
geompy.addToStudy( myPermanentMagnet, 'myPermanentMagnet' )
geompy.addToStudy( myAirBox, 'myAirBox' )
geompy.addToStudy( combinedGeometry, 'combinedGeometry' )
geompy.addToStudyInFather( combinedGeometry, furnace, 'furnace' )
geompy.addToStudyInFather( combinedGeometry, magnet, 'magnet' )
geompy.addToStudyInFather( combinedGeometry, air, 'air' )
geompy.addToStudyInFather( combinedGeometry, outer, 'outer' )
geompy.addToStudyInFather( combinedGeometry, furnacePlusX, 'furnacePlusX' )
geompy.addToStudyInFather( combinedGeometry, furnaceMinusX, 'furnaceMinusX' )
geompy.addToStudyInFather( combinedGeometry, funacePlusY, 'funacePlusY' )
geompy.addToStudyInFather( combinedGeometry, furnaceMinusY, 'furnaceMinusY' )
geompy.addToStudyInFather( combinedGeometry, furnacePlusZ, 'furnacePlusZ' )
geompy.addToStudyInFather( combinedGeometry, furnaceMinusZ, 'furnaceMinusZ' )
## Geometry that will be hidden from the study
geompy.hideInStudy(rotationVector1)
geompy.hideInStudy(rotationVector2)

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)
## Definition of mesh
exampleBoxMesh = smesh.Mesh(combinedGeometry)
NETGEN_1D_2D_3D = exampleBoxMesh.Tetrahedron(algo=smeshBuilder.NETGEN_1D2D3D)
NETGEN3DmeshingParameters = NETGEN_1D_2D_3D.Parameters()
NETGEN3DmeshingParameters.SetMaxSize( maxGeomSize )
NETGEN3DmeshingParameters.SetMinSize( minGeomSize )
NETGEN3DmeshingParameters.SetSecondOrder( 0 )
NETGEN3DmeshingParameters.SetOptimize( 1 )
## Set Fineness
NETGEN3DmeshingParameters.SetFineness( globalMeshFineness )
## Custom parameters (if fineness == 5)
##NETGEN3DmeshingParameters.SetGrowthRate( 0.3 )
##NETGEN3DmeshingParameters.SetNbSegPerEdge( 1 )
##NETGEN3DmeshingParameters.SetNbSegPerRadius( 2 )
##
## ?? 0 or 1
##NETGEN3DmeshingParameters.SetCheckChartBoundary( 192 )
##NETGEN3DmeshingParameters.SetCheckChartBoundary( 0 )
##
NETGEN3DmeshingParameters.SetChordalError( -1 )
NETGEN3DmeshingParameters.SetChordalErrorEnabled( 0 )
NETGEN3DmeshingParameters.SetUseSurfaceCurvature( 1 )
NETGEN3DmeshingParameters.SetFuseEdges( 1 )
NETGEN3DmeshingParameters.SetQuadAllowed( 0 )
NETGEN3DmeshingParameters.UnsetLocalSizeOnEntry("combinedGeometry")
## Set air domain mesh to the coarsest size
NETGEN3DmeshingParameters.SetLocalSizeOnShape(air, maxGeomSize)
NETGEN3DmeshingParameters.SetLocalSizeOnShape(outer, maxGeomSize)
## Set magnet domain mesh size
NETGEN3DmeshingParameters.SetLocalSizeOnShape(magnet, magnetGeomSize)
## Set furnace domain mesh size
NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnace, furnaceGeomSize)
NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnaceMinusX, furnaceGeomSize)
NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnacePlusX, furnaceGeomSize)
NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnaceMinusY, furnaceGeomSize)
NETGEN3DmeshingParameters.SetLocalSizeOnShape(funacePlusY, furnaceGeomSize)
NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnaceMinusZ, furnaceGeomSize)
NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnacePlusZ, furnaceGeomSize)
## Set mesh of the closest face to the magnet to the finest size
if magnetOffsetX < -furnaceDimX/2: NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnaceMinusX, minGeomSize)
if magnetOffsetX > furnaceDimX/2: NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnacePlusX, minGeomSize)
if magnetOffsetY < -furnaceDimY/2: NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnaceMinusY, minGeomSize)
if magnetOffsetY > furnaceDimY/2: NETGEN3DmeshingParameters.SetLocalSizeOnShape(funacePlusY, minGeomSize)
if magnetOffsetZ < -furnaceDimZ/2: NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnaceMinusZ, minGeomSize)
if magnetOffsetZ > furnaceDimZ/2: NETGEN3DmeshingParameters.SetLocalSizeOnShape(furnacePlusZ, minGeomSize)
##
NETGEN3DmeshingParameters.UnsetLocalSizeOnEntry("combinedGeometry")
## Generate mesh
isDone = exampleBoxMesh.Compute()
## Create groups from geometry for Elmer
furnace_1 = exampleBoxMesh.GroupOnGeom(furnace,'furnace',SMESH.VOLUME)
magnet_1 = exampleBoxMesh.GroupOnGeom(magnet,'magnet',SMESH.VOLUME)
air_1 = exampleBoxMesh.GroupOnGeom(air,'air',SMESH.VOLUME)
outer_1 = exampleBoxMesh.GroupOnGeom(outer,'outer',SMESH.FACE)

## Set names of Mesh objects
smesh.SetName(NETGEN_1D_2D_3D.GetAlgorithm(), 'NETGEN 1D-2D-3D')
smesh.SetName(magnet_1, 'magnet')
smesh.SetName(outer_1, 'outer')
smesh.SetName(air_1, 'air')
smesh.SetName(furnace_1, 'furnace')
smesh.SetName(exampleBoxMesh.GetMesh(), 'exampleBoxMesh')
smesh.SetName(NETGEN3DmeshingParameters, 'NETGEN3DmeshingParameters')

## Mesh export
import os
## Path to script salomeToElmerAuto
sys.path.append(os.getcwd())
## Get Export function
from salomeToElmerAuto import exportToElmer

## Define export directory
#outdir=os.getcwd()+"/"+exampleBoxMesh.GetName()
outdir=os.getcwd()+"/meshElmer"
print("Exporting mesh to " + outdir)
## Export mesh
exportToElmer(exampleBoxMesh,outdir)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

os.system("rm -rf __pycache__")
