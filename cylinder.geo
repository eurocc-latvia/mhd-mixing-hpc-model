// Gmsh allows variables; these will be used to set desired element sizes at various Points

// Furnace characteristic dimensions
x0 = 0.0465/2;//	+z0 |```````````````|
y0 = 0.0465/2;//		|				|
z0 = 0.07/2;//	-z0 |_______________|

x0=x0+1e-4;
y0=y0+1e-4;
z0=z0+1e-4;

z1 = -z0;//		-x0	-y0			 +x0 +y0
radius = (x0+y0)/2;
cubesize = 0.3*radius;
rotx = 0;
roty = 0;
rotz = 1;
rotangle = 45/180*Pi;

globalMeshFineness = 3 ;
numradial = Max(10,5*Round(Max(x0,y0)/Min(x0,Min(y0,z0)))*(1+globalMeshFineness)); //mesh elements in r
numarc =	Max(10,2*Round(Max(x0,y0)/Min(x0,Min(y0,z0)))*(1+globalMeshFineness)); //mesh elements in theta
numaxial =	Max(10,20*Round(		   z0/Min(x0,Min(y0,z0)))*(1+globalMeshFineness)); //mesh elements in z

numradial=numradial+1;
numarc=numarc+1;
numaxial=numaxial+1;
frequency = 400;
magnet_rad = 0.02;
magnet_x = 0;
magnet_y = 0;
magnet_z = -0.065;
utau = frequency*Sqrt((Abs(magnet_x)-magnet_rad-x0)^2+(Abs(magnet_y)-magnet_rad-y0)^2+(Abs(magnet_z)-magnet_rad-z0)^2);//characteristic flow velocity
nu = 3.75e-7;//liquid metal viscosity
yplus = 330.0 ;//desired y+ for the chosen turbulence model
cl3 = 2.0*x0/numradial;//default element size at given points
first = nu*yplus/utau;//first wall element size
axialBump = 1.0/(2.0*cl3/first-1.0);//smallest-to-largest mesh element ratio
//radialProg=1-7.5/numradial;//neighbouring element growth ratio
radialProg=0.9;//neighbouring element growth ratio

//exterior (bounding box) of mesh
Point(1) = { 0, -cubesize, z1, cl3};
Point(2) = { cubesize,  0, z1, cl3};
Point(3) = { 0,  cubesize, z1, cl3};
Point(4) = {-cubesize,  0, z1, cl3};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Point(5) = {0,		  0, z1, cl3};
Point(8) = {0,   radius, z1, cl3};
Point(9) = {0,  -radius, z1, cl3};
Point(12) = {radius,  0, z1, cl3};
Point(13) = {-radius, 0, z1, cl3};

Circle(7) = {8, 5, 13};
Circle(8) = {9, 5, 12};

Line(9)  = {3, 8};
Line(10) = {1, 9};
Line(11) = {2, 12};
Line(12) = {4, 13};

Line(13) = {2, 3};
Line(14) = {4, 1};
Circle(15) = {13, 5, 9};
Circle(16) = {12, 5, 8};

Line Loop(1) = {1, 2, 3, 4}; 
Line Loop(2) = {10, 8, -11, -1}; 
Line Loop(3) = {7, -12, -3, 9}; 
Line Loop(4) = {-10, -14, 12, 15}; 
Line Loop(5) = {16, -9, -13, 11}; 

Plane Surface(1) = {1}; 
Plane Surface(2) = {2}; 
Plane Surface(3) = {3}; 
Plane Surface(4) = {4}; 
Plane Surface(5) = {5}; 

ext1[] = Extrude {0, 0, 2*z0} { Surface{1};};
ext2[] = Extrude {0, 0, 2*z0} { Surface{2};};
ext3[] = Extrude {0, 0, 2*z0} { Surface{3};};
ext4[] = Extrude {0, 0, 2*z0} { Surface{4};};
ext5[] = Extrude {0, 0, 2*z0} { Surface{5};};


// Mesh these surfaces in a structured manner
Transfinite Line {7,8,13,14,15,16} = numarc; 
Transfinite Line {9,10,11,12} = numradial Using Progression radialProg;    
Transfinite Line {1,2,3,4} = numarc;// Using Progression ;//

Transfinite Line {41,87,62,106} = numarc; 
Transfinite Line {40,-42,-63,65} = numradial Using Progression radialProg;    
Transfinite Line {18,19,20,21} = numarc;// Using Progression ;//


Transfinite Line {46,68,67,50,24,23,32,28} = numaxial Using Bump axialBump;
Transfinite Volume "*";
Recombine Volume "*";
Transfinite Surface "*";
Recombine Surface "*";

Rotate {{rotx,roty,rotz}, {0,0,0}, rotangle} {Volume{ext1[1],ext2[1],ext3[1],ext4[1],ext5[1]};}

Physical Surface("front") = {ext2[3]};
Physical Surface("back") = {ext3[2]};
Physical Surface("left") = {ext4[5]};
Physical Surface("top") = {ext1[0],ext2[0],ext3[0],ext4[0],ext5[0]};
Physical Surface("right") = {ext5[2]};
Physical Surface("bot") = {1,2,3,4,5};
Physical Volume("fullbox") = {ext1[1],ext2[1],ext3[1],ext4[1],ext5[1]};
