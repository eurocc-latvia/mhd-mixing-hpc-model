#import os
#import sys
#sys.path.insert(0, '/nfs/root/software/anaconda3-2020/pkgs/imageio-2.9.0-py_0/site-packages')
#sys.path.insert(0, '/nfs/root/software/anaconda3-2020/pkgs/pillow-8.0.1-py38he98fc37_0/lib/python3.8/site-packages')
#import imageio
#
##png_dir = './animation'
#png_dir = './'
#images = []
#for file_name in sorted(os.listdir(png_dir)):
#    if file_name.endswith('.png'):
#        file_path = os.path.join(png_dir, file_name)
#        images.append(imageio.imread(file_path))
#imageio.mimsave('./post.gif', images)# trace generated using paraview version 5.8.0
#

furnaceDimX = 4.000
furnaceDimY = 2
furnaceDimZ = 0.6
maxXYZ = max(furnaceDimX, furnaceDimY, furnaceDimZ)
cam123 = maxXYZ*3.4

#### import the simple module from the paraview
from paraview.simple import *

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
hpc = OpenFOAMReader(FileName='./hpc')

# get animation scene
animationScene1 = GetAnimationScene()

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1508, 690]

# get layout
layout1 = GetLayout()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Properties modified on hpc
hpc.SkipZeroTime = 0

# get display properties
hpcDisplay = GetDisplayProperties(hpc, view=renderView1)

# turn off scalar coloring
ColorBy(hpcDisplay, None)

# change representation type
hpcDisplay.SetRepresentationType('Feature Edges')

# create a new 'Temporal Interpolator'
temporalInterpolator1 = TemporalInterpolator(Input=hpc)

# create a new 'Calculator'
calculator1 = Calculator(Input=temporalInterpolator1)

# Properties modified on calculator1 where 1000 is a scale for liters
calculator1.Function = '1000*abs(U_X)'

# Properties modified on temporalInterpolator1
temporalInterpolator1.DiscreteTimeStepInterval = 1.0

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# Properties modified on calculator1
calculator1.ResultArrayName = 'flowrate'

# create a new 'Slice'
slice1 = Slice(Input=calculator1)

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'

# get color transfer function/color map for 'flowrate'
flowrateLUT = GetColorTransferFunction('flowrate')

# get opacity transfer function/opacity map for 'flowrate'
flowratePWF = GetOpacityTransferFunction('flowrate')

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [-furnaceDimX*0.4, 0.0, 0.0]

# Rescale transfer function
flowrateLUT.RescaleTransferFunction(0.0, 0.5)

# Rescale transfer function
flowratePWF.RescaleTransferFunction(0.0, 0.5)

# create a new 'Slice'
slice2 = Slice(Input=calculator1)

# show data in view
slice2Display = Show(slice2, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice2Display.Representation = 'Surface'

# create a new 'Slice'
slice3 = Slice(Input=calculator1)

# show data in view
slice3Display = Show(slice3, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice3Display.Representation = 'Surface'

# Properties modified on slice3.SliceType
slice3.SliceType.Origin = [furnaceDimX*0.4, 0.0, 0.0]

# create a new 'Integrate Variables'
integrateVariables1 = IntegrateVariables(Input=slice1)

# create a new 'Integrate Variables'
integrateVariables2 = IntegrateVariables(Input=slice2)

# create a new 'Integrate Variables'
integrateVariables3 = IntegrateVariables(Input=slice3)

animationScene1.GoToLast()

# create a new 'Group Datasets'
groupDatasets1 = GroupDatasets(Input=[integrateVariables1, integrateVariables2, integrateVariables3])

# create a new 'Plot Data Over Time'
plotDataOverTime1 = PlotDataOverTime(Input=groupDatasets1)

# Create a new 'Quartile Chart View'
quartileChartView1 = CreateView('QuartileChartView')
# uncomment following to set a specific view size
# quartileChartView1.ViewSize = [400, 400]

# show data in view
plotDataOverTime1Display = Show(plotDataOverTime1, quartileChartView1, 'QuartileChartRepresentation')

# add view to a layout so it's visible in UI
AssignViewToLayout(view=quartileChartView1, layout=layout1, hint=0)

# Properties modified on plotDataOverTime1Display
plotDataOverTime1Display.SeriesColor = ['B (0) ( block=1)', '0', '0', '0', 'B (1) ( block=1)', '0.889998', '0.100008', '0.110002', 'B (2) ( block=1)', '0.220005', '0.489998', '0.719997', 'B (Magnitude) ( block=1)', '0.300008', '0.689998', '0.289998', 'flowrate ( block=1)', '1', '0', '0', 'J (0) ( block=1)', '1', '0.500008', '0', 'J (1) ( block=1)', '0.650004', '0.340002', '0.160006', 'J (2) ( block=1)', '0', '0', '0', 'J (Magnitude) ( block=1)', '0.889998', '0.100008', '0.110002', 'k ( block=1)', '0.220005', '0.489998', '0.719997', 'nut ( block=1)', '0.300008', '0.689998', '0.289998', 'omega ( block=1)', '0.6', '0.310002', '0.639994', 'p ( block=1)', '1', '0.500008', '0', 'U (0) ( block=1)', '0.650004', '0.340002', '0.160006', 'U (1) ( block=1)', '0', '0', '0', 'U (2) ( block=1)', '0.889998', '0.100008', '0.110002', 'U (Magnitude) ( block=1)', '0.220005', '0.489998', '0.719997', 'X ( block=1)', '0.300008', '0.689998', '0.289998', 'Y ( block=1)', '0.6', '0.310002', '0.639994', 'Z ( block=1)', '1', '0.500008', '0', 'N ( block=1)', '0.650004', '0.340002', '0.160006', 'Time ( block=1)', '0', '0', '0', 'vtkValidPointMask ( block=1)', '0.889998', '0.100008', '0.110002', 'B (0) ( block=2)', '0.220005', '0.489998', '0.719997', 'B (1) ( block=2)', '0.300008', '0.689998', '0.289998', 'B (2) ( block=2)', '0.6', '0.310002', '0.639994', 'B (Magnitude) ( block=2)', '1', '0.500008', '0', 'flowrate ( block=2)', '0', '1', '0', 'J (0) ( block=2)', '0', '0', '0', 'J (1) ( block=2)', '0.889998', '0.100008', '0.110002', 'J (2) ( block=2)', '0.220005', '0.489998', '0.719997', 'J (Magnitude) ( block=2)', '0.300008', '0.689998', '0.289998', 'k ( block=2)', '0.6', '0.310002', '0.639994', 'nut ( block=2)', '1', '0.500008', '0', 'omega ( block=2)', '0.650004', '0.340002', '0.160006', 'p ( block=2)', '0', '0', '0', 'U (0) ( block=2)', '0.889998', '0.100008', '0.110002', 'U (1) ( block=2)', '0.220005', '0.489998', '0.719997', 'U (2) ( block=2)', '0.300008', '0.689998', '0.289998', 'U (Magnitude) ( block=2)', '0.6', '0.310002', '0.639994', 'X ( block=2)', '1', '0.500008', '0', 'Y ( block=2)', '0.650004', '0.340002', '0.160006', 'Z ( block=2)', '0', '0', '0', 'N ( block=2)', '0.889998', '0.100008', '0.110002', 'Time ( block=2)', '0.220005', '0.489998', '0.719997', 'vtkValidPointMask ( block=2)', '0.300008', '0.689998', '0.289998']
plotDataOverTime1Display.SeriesPlotCorner = ['B (0) ( block=1)', '0', 'B (0) ( block=2)', '0', 'B (1) ( block=1)', '0', 'B (1) ( block=2)', '0', 'B (2) ( block=1)', '0', 'B (2) ( block=2)', '0', 'B (Magnitude) ( block=1)', '0', 'B (Magnitude) ( block=2)', '0', 'J (0) ( block=1)', '0', 'J (0) ( block=2)', '0', 'J (1) ( block=1)', '0', 'J (1) ( block=2)', '0', 'J (2) ( block=1)', '0', 'J (2) ( block=2)', '0', 'J (Magnitude) ( block=1)', '0', 'J (Magnitude) ( block=2)', '0', 'N ( block=1)', '0', 'N ( block=2)', '0', 'Time ( block=1)', '0', 'Time ( block=2)', '0', 'U (0) ( block=1)', '0', 'U (0) ( block=2)', '0', 'U (1) ( block=1)', '0', 'U (1) ( block=2)', '0', 'U (2) ( block=1)', '0', 'U (2) ( block=2)', '0', 'U (Magnitude) ( block=1)', '0', 'U (Magnitude) ( block=2)', '0', 'X ( block=1)', '0', 'X ( block=2)', '0', 'Y ( block=1)', '0', 'Y ( block=2)', '0', 'Z ( block=1)', '0', 'Z ( block=2)', '0', 'flowrate ( block=1)', '0', 'flowrate ( block=2)', '0', 'k ( block=1)', '0', 'k ( block=2)', '0', 'nut ( block=1)', '0', 'nut ( block=2)', '0', 'omega ( block=1)', '0', 'omega ( block=2)', '0', 'p ( block=1)', '0', 'p ( block=2)', '0', 'vtkValidPointMask ( block=1)', '0', 'vtkValidPointMask ( block=2)', '0']
plotDataOverTime1Display.SeriesLineStyle = ['B (0) ( block=1)', '1', 'B (0) ( block=2)', '1', 'B (1) ( block=1)', '1', 'B (1) ( block=2)', '1', 'B (2) ( block=1)', '1', 'B (2) ( block=2)', '1', 'B (Magnitude) ( block=1)', '1', 'B (Magnitude) ( block=2)', '1', 'J (0) ( block=1)', '1', 'J (0) ( block=2)', '1', 'J (1) ( block=1)', '1', 'J (1) ( block=2)', '1', 'J (2) ( block=1)', '1', 'J (2) ( block=2)', '1', 'J (Magnitude) ( block=1)', '1', 'J (Magnitude) ( block=2)', '1', 'N ( block=1)', '1', 'N ( block=2)', '1', 'Time ( block=1)', '1', 'Time ( block=2)', '1', 'U (0) ( block=1)', '1', 'U (0) ( block=2)', '1', 'U (1) ( block=1)', '1', 'U (1) ( block=2)', '1', 'U (2) ( block=1)', '1', 'U (2) ( block=2)', '1', 'U (Magnitude) ( block=1)', '1', 'U (Magnitude) ( block=2)', '1', 'X ( block=1)', '1', 'X ( block=2)', '1', 'Y ( block=1)', '1', 'Y ( block=2)', '1', 'Z ( block=1)', '1', 'Z ( block=2)', '1', 'flowrate ( block=1)', '1', 'flowrate ( block=2)', '1', 'k ( block=1)', '1', 'k ( block=2)', '1', 'nut ( block=1)', '1', 'nut ( block=2)', '1', 'omega ( block=1)', '1', 'omega ( block=2)', '1', 'p ( block=1)', '1', 'p ( block=2)', '1', 'vtkValidPointMask ( block=1)', '1', 'vtkValidPointMask ( block=2)', '1']
plotDataOverTime1Display.SeriesLineThickness = ['B (0) ( block=1)', '2', 'B (0) ( block=2)', '2', 'B (1) ( block=1)', '2', 'B (1) ( block=2)', '2', 'B (2) ( block=1)', '2', 'B (2) ( block=2)', '2', 'B (Magnitude) ( block=1)', '2', 'B (Magnitude) ( block=2)', '2', 'J (0) ( block=1)', '2', 'J (0) ( block=2)', '2', 'J (1) ( block=1)', '2', 'J (1) ( block=2)', '2', 'J (2) ( block=1)', '2', 'J (2) ( block=2)', '2', 'J (Magnitude) ( block=1)', '2', 'J (Magnitude) ( block=2)', '2', 'N ( block=1)', '2', 'N ( block=2)', '2', 'Time ( block=1)', '2', 'Time ( block=2)', '2', 'U (0) ( block=1)', '2', 'U (0) ( block=2)', '2', 'U (1) ( block=1)', '2', 'U (1) ( block=2)', '2', 'U (2) ( block=1)', '2', 'U (2) ( block=2)', '2', 'U (Magnitude) ( block=1)', '2', 'U (Magnitude) ( block=2)', '2', 'X ( block=1)', '2', 'X ( block=2)', '2', 'Y ( block=1)', '2', 'Y ( block=2)', '2', 'Z ( block=1)', '2', 'Z ( block=2)', '2', 'flowrate ( block=1)', '2', 'flowrate ( block=2)', '2', 'k ( block=1)', '2', 'k ( block=2)', '2', 'nut ( block=1)', '2', 'nut ( block=2)', '2', 'omega ( block=1)', '2', 'omega ( block=2)', '2', 'p ( block=1)', '2', 'p ( block=2)', '2', 'vtkValidPointMask ( block=1)', '2', 'vtkValidPointMask ( block=2)', '2']
plotDataOverTime1Display.SeriesMarkerStyle = ['B (0) ( block=1)', '0', 'B (0) ( block=2)', '0', 'B (1) ( block=1)', '0', 'B (1) ( block=2)', '0', 'B (2) ( block=1)', '0', 'B (2) ( block=2)', '0', 'B (Magnitude) ( block=1)', '0', 'B (Magnitude) ( block=2)', '0', 'J (0) ( block=1)', '0', 'J (0) ( block=2)', '0', 'J (1) ( block=1)', '0', 'J (1) ( block=2)', '0', 'J (2) ( block=1)', '0', 'J (2) ( block=2)', '0', 'J (Magnitude) ( block=1)', '0', 'J (Magnitude) ( block=2)', '0', 'N ( block=1)', '0', 'N ( block=2)', '0', 'Time ( block=1)', '0', 'Time ( block=2)', '0', 'U (0) ( block=1)', '0', 'U (0) ( block=2)', '0', 'U (1) ( block=1)', '0', 'U (1) ( block=2)', '0', 'U (2) ( block=1)', '0', 'U (2) ( block=2)', '0', 'U (Magnitude) ( block=1)', '0', 'U (Magnitude) ( block=2)', '0', 'X ( block=1)', '0', 'X ( block=2)', '0', 'Y ( block=1)', '0', 'Y ( block=2)', '0', 'Z ( block=1)', '0', 'Z ( block=2)', '0', 'flowrate ( block=1)', '0', 'flowrate ( block=2)', '0', 'k ( block=1)', '0', 'k ( block=2)', '0', 'nut ( block=1)', '0', 'nut ( block=2)', '0', 'omega ( block=1)', '0', 'omega ( block=2)', '0', 'p ( block=1)', '0', 'p ( block=2)', '0', 'vtkValidPointMask ( block=1)', '0', 'vtkValidPointMask ( block=2)', '0']
plotDataOverTime1Display.SeriesMarkerSize = ['B (0) ( block=1)', '4', 'B (0) ( block=2)', '4', 'B (1) ( block=1)', '4', 'B (1) ( block=2)', '4', 'B (2) ( block=1)', '4', 'B (2) ( block=2)', '4', 'B (Magnitude) ( block=1)', '4', 'B (Magnitude) ( block=2)', '4', 'J (0) ( block=1)', '4', 'J (0) ( block=2)', '4', 'J (1) ( block=1)', '4', 'J (1) ( block=2)', '4', 'J (2) ( block=1)', '4', 'J (2) ( block=2)', '4', 'J (Magnitude) ( block=1)', '4', 'J (Magnitude) ( block=2)', '4', 'N ( block=1)', '4', 'N ( block=2)', '4', 'Time ( block=1)', '4', 'Time ( block=2)', '4', 'U (0) ( block=1)', '4', 'U (0) ( block=2)', '4', 'U (1) ( block=1)', '4', 'U (1) ( block=2)', '4', 'U (2) ( block=1)', '4', 'U (2) ( block=2)', '4', 'U (Magnitude) ( block=1)', '4', 'U (Magnitude) ( block=2)', '4', 'X ( block=1)', '4', 'X ( block=2)', '4', 'Y ( block=1)', '4', 'Y ( block=2)', '4', 'Z ( block=1)', '4', 'Z ( block=2)', '4', 'flowrate ( block=1)', '4', 'flowrate ( block=2)', '4', 'k ( block=1)', '4', 'k ( block=2)', '4', 'nut ( block=1)', '4', 'nut ( block=2)', '4', 'omega ( block=1)', '4', 'omega ( block=2)', '4', 'p ( block=1)', '4', 'p ( block=2)', '4', 'vtkValidPointMask ( block=1)', '4', 'vtkValidPointMask ( block=2)', '4']

# Properties modified on plotDataOverTime1Display
plotDataOverTime1Display.CompositeDataSetIndex = [1, 2, 3]

# Properties modified on plotDataOverTime1Display
plotDataOverTime1Display.SeriesVisibility = ['flowrate ( block=1)', 'flowrate ( block=2)', 'flowrate ( block=3)']

# Properties modified on plotDataOverTime1Display
plotDataOverTime1Display.SeriesLabel = ['B (0) ( block=1)', 'B (0) ( block=1)', 'B (1) ( block=1)', 'B (1) ( block=1)', 'B (2) ( block=1)', 'B (2) ( block=1)', 'B (Magnitude) ( block=1)', 'B (Magnitude) ( block=1)', 'flowrate ( block=1)', 'Section1 at x=-1 m', 'J (0) ( block=1)', 'J (0) ( block=1)', 'J (1) ( block=1)', 'J (1) ( block=1)', 'J (2) ( block=1)', 'J (2) ( block=1)', 'J (Magnitude) ( block=1)', 'J (Magnitude) ( block=1)', 'k ( block=1)', 'k ( block=1)', 'nut ( block=1)', 'nut ( block=1)', 'omega ( block=1)', 'omega ( block=1)', 'p ( block=1)', 'p ( block=1)', 'U (0) ( block=1)', 'U (0) ( block=1)', 'U (1) ( block=1)', 'U (1) ( block=1)', 'U (2) ( block=1)', 'U (2) ( block=1)', 'U (Magnitude) ( block=1)', 'U (Magnitude) ( block=1)', 'X ( block=1)', 'X ( block=1)', 'Y ( block=1)', 'Y ( block=1)', 'Z ( block=1)', 'Z ( block=1)', 'N ( block=1)', 'N ( block=1)', 'Time ( block=1)', 'Time ( block=1)', 'vtkValidPointMask ( block=1)', 'vtkValidPointMask ( block=1)', 'B (0) ( block=2)', 'B (0) ( block=2)', 'B (1) ( block=2)', 'B (1) ( block=2)', 'B (2) ( block=2)', 'B (2) ( block=2)', 'B (Magnitude) ( block=2)', 'B (Magnitude) ( block=2)', 'flowrate ( block=2)', 'Section2 at x= 0 m', 'J (0) ( block=2)', 'J (0) ( block=2)', 'J (1) ( block=2)', 'J (1) ( block=2)', 'J (2) ( block=2)', 'J (2) ( block=2)', 'J (Magnitude) ( block=2)', 'J (Magnitude) ( block=2)', 'k ( block=2)', 'k ( block=2)', 'nut ( block=2)', 'nut ( block=2)', 'omega ( block=2)', 'omega ( block=2)', 'p ( block=2)', 'p ( block=2)', 'U (0) ( block=2)', 'U (0) ( block=2)', 'U (1) ( block=2)', 'U (1) ( block=2)', 'U (2) ( block=2)', 'U (2) ( block=2)', 'U (Magnitude) ( block=2)', 'U (Magnitude) ( block=2)', 'X ( block=2)', 'X ( block=2)', 'Y ( block=2)', 'Y ( block=2)', 'Z ( block=2)', 'Z ( block=2)', 'N ( block=2)', 'N ( block=2)', 'Time ( block=2)', 'Time ( block=2)', 'vtkValidPointMask ( block=2)', 'vtkValidPointMask ( block=2)', 'B (0) ( block=3)', 'B (0) ( block=3)', 'B (1) ( block=3)', 'B (1) ( block=3)', 'B (2) ( block=3)', 'B (2) ( block=3)', 'B (Magnitude) ( block=3)', 'B (Magnitude) ( block=3)', 'flowrate ( block=3)', 'Section3 at x= 1 m', 'J (0) ( block=3)', 'J (0) ( block=3)', 'J (1) ( block=3)', 'J (1) ( block=3)', 'J (2) ( block=3)', 'J (2) ( block=3)', 'J (Magnitude) ( block=3)', 'J (Magnitude) ( block=3)', 'k ( block=3)', 'k ( block=3)', 'nut ( block=3)', 'nut ( block=3)', 'omega ( block=3)', 'omega ( block=3)', 'p ( block=3)', 'p ( block=3)', 'U (0) ( block=3)', 'U (0) ( block=3)', 'U (1) ( block=3)', 'U (1) ( block=3)', 'U (2) ( block=3)', 'U (2) ( block=3)', 'U (Magnitude) ( block=3)', 'U (Magnitude) ( block=3)', 'X ( block=3)', 'X ( block=3)', 'Y ( block=3)', 'Y ( block=3)', 'Z ( block=3)', 'Z ( block=3)', 'N ( block=3)', 'N ( block=3)', 'Time ( block=3)', 'Time ( block=3)', 'vtkValidPointMask ( block=3)', 'vtkValidPointMask ( block=3)']

# Properties modified on quartileChartView1
#quartileChartView1.ChartTitle = 'Absolute flowrate through different cross sections over time'

# Properties modified on quartileChartView1
quartileChartView1.LeftAxisTitle = 'Absolute flowrate, L/s'

# Properties modified on quartileChartView1
quartileChartView1.BottomAxisTitle = 'Time, s'

# set scalar coloring
ColorBy(slice1Display, ('POINTS', 'U', 'Magnitude'))

# rescale color and/or opacity maps used to include current data range
slice1Display.RescaleTransferFunctionToDataRange(True, False)

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')

# set scalar coloring
ColorBy(slice2Display, ('POINTS', 'U', 'Magnitude'))

# rescale color and/or opacity maps used to include current data range
slice2Display.RescaleTransferFunctionToDataRange(True, False)

# set scalar coloring
ColorBy(slice3Display, ('POINTS', 'U', 'Magnitude'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(flowrateLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
slice3Display.RescaleTransferFunctionToDataRange(True, False)

# create a new 'Calculator'
calculator2 = Calculator(Input=temporalInterpolator1)

# Properties modified on calculator2
calculator2.ResultArrayName = 'velocity'

# Properties modified on calculator2
calculator2.Function = 'mag(U)'

# create a new 'Glyph'
glyph4 = Glyph(Input=calculator2, GlyphType='Arrow')

# show data in view
glyph4Display = Show(glyph4, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
glyph4Display.Representation = 'Surface'

# show color bar/color legend
glyph4Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on glyph4
glyph4.OrientationArray = ['POINTS', 'U']

# Properties modified on glyph4
glyph4.ScaleArray = ['POINTS', 'U']

# Properties modified on glyph4
glyph4.ScaleFactor = maxXYZ*0.075

# Properties modified on glyph4
glyph4.MaximumNumberOfSamplePoints = 3000

# set scalar coloring
ColorBy(glyph4Display, ('POINTS', 'velocity'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(flowrateLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
glyph4Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
glyph4Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'velocity'
velocityLUT = GetColorTransferFunction('velocity')

# get opacity transfer function/opacity map for 'velocity'
velocityPWF = GetOpacityTransferFunction('velocity')

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# Properties modified on slice1Display
slice1Display.Opacity = 0.4

# show data in view
slice2Display = Show(slice2, renderView1, 'GeometryRepresentation')

# Properties modified on slice2Display
slice2Display.Opacity = 0.4

# show data in view
slice3Display = Show(slice3, renderView1, 'GeometryRepresentation')

# Properties modified on slice3Display
slice3Display.Opacity = 0.4

# hide color bar/color legend
slice3Display.SetScalarBarVisibility(renderView1, False)

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(Input=calculator1)

# show data in view
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1, 'TextSourceRepresentation')

# Properties modified on annotateTimeFilter1
annotateTimeFilter1.Format = 'f=1Hz, time: %0.1f s'

# update the view to ensure updated data information
renderView1.Update()

# current camera placement for renderView1
renderView1.CameraPosition = [-4.83/9*cam123, -4.31/9*cam123, 4.69/9*cam123]
renderView1.CameraFocalPoint = [0, 0, 0]
renderView1.CameraViewUp = [0.4/9*cam123, 0.4/9*cam123, 0.8/9*cam123]
renderView1.CameraParallelScale = cam123/4

# save screenshot
SaveScreenshot('./flowrate.png', layout1, SaveAllViews=1,
    ImageResolution=[1920, 1080])

# save animation
SaveAnimation('./flowrate.avi', layout1, SaveAllViews=1,
   ImageResolution=[1920, 1080],
   FrameRate=10,
   FrameWindow=[0,999])

#import os
#import imageio
#
##png_dir = './animation'
#png_dir = ''
#images = []
#for file_name in sorted(os.listdir(png_dir)):
#    if file_name.endswith('.png'):
#        file_path = os.path.join(png_dir, file_name)
#        images.append(imageio.imread(file_path))
#imageio.mimsave('./post.gif', images)# trace generated using paraview version 5.8.0
