// Gmsh allows variables; these will be used to set desired element sizes at various Points

// Furnace characteristic dimensions
x0 = 0.1/2;//	+z0 |````````````````````/
y0 = 0.1/2;//		|					/
z0 = 0.1/2;//	-z0 |__________________/
y1 = y0+1.65;//		-y0		    		+y0  +y1
//rotx = 1;
//roty = 0;
//rotz = 0;
//rotangle = -90/180*Pi;

globalMeshFineness = 4 ;
nx = Max(10,5*Round(x0/Min(x0,Min(y0,z0)))*(1+globalMeshFineness)); //mesh elements in x
ny = Max(10,5*Round(y0/Min(x0,Min(y0,z0)))*(1+globalMeshFineness)); //mesh elements in y
nz = Max(10,5*Round(z0/Min(x0,Min(y0,z0)))*(1+globalMeshFineness)); //mesh elements in z

Nx=nx+1;
Ny=ny+1;
Nz=nz+1;
frequency = 20;
magnet_rad = 0.025;
magnet_x = 0;
magnet_y = 0.0;
magnet_z = -0.1;
utau = frequency*Sqrt((Abs(magnet_x)-magnet_rad-x0)^2+(Abs(magnet_y)-magnet_rad-y0)^2+(Abs(magnet_z)-magnet_rad-z0)^2);//characteristic flow velocity
nu = 3.75e-7;//liquid metal viscosity
yplus = 330;//desired y+ for the chosen turbulence model
cl3 = 2.0*x0/Nx;//default element size at given points
first = nu*yplus/utau;//first wall element size
bump = 1.0/(2.0*cl3/first-1.0);//smallest-to-largest mesh element ratio
r=1.2;//neighbouring element growth ratio

//exterior (bounding box) of mesh
Point(1) = { -x0, -y0, -z0, cl3};
Point(2) = { -x0, y0, -z0, cl3};
Point(3) = { -x0, y1,  z0, cl3};
Point(4) = { -x0, -y0,  z0, cl3};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(1) = {1, 2, 3, 4}; // Exterior
Plane Surface(1) = {1}; // Outer unstructured region

//refinement towards the sides
ext1[] = Extrude {2*x0, 0, 0} { Surface{1};};
Transfinite Line {11,12,16,20} = Nx Using Bump bump;//Progression r;//
Transfinite Line { 1, 3, 6, 8} = Ny Using Bump bump;//Progression r;//
Transfinite Line { 2, 4, 7, 9} = Nz Using Bump bump;//Progression r;//
Transfinite Volume "*";
Recombine Volume "*";
Transfinite Surface "*";
Recombine Surface "*";

//Rotate {{rotx,roty,rotz}, {0,0,0}, rotangle} {Volume{ext1[1]};}

Physical Surface("front") = {1};
Physical Surface("back") = {ext1[0]};
Physical Surface("left") = {ext1[2]};
Physical Surface("top") = {ext1[3]};
Physical Surface("right") = {ext1[4]};
Physical Surface("bot") = {ext1[5]};
Physical Volume("fullbox") = {ext1[1]};
