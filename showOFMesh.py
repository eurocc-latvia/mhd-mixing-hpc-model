# trace generated using paraview version 5.8.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

furnaceDimX = 4.000
furnaceDimY = 1.246
furnaceDimZ = 0.570
furnaceDimX1 = 0.53
furnaceDimX2 = 0.9
furnaceDimY1 = 1.1
furnaceDimY2 = 1.1
maxXYZ = max(furnaceDimX, furnaceDimY, furnaceDimZ)
#maxXYZ = max(furnaceDimX+furnaceDimX1+furnaceDimX2, furnaceDimY+furnaceDimY1+furnaceDimY2, furnaceDimZ)
cam123 = maxXYZ*3.4


#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'Reader'
showMesh = XMLUnstructuredGridReader(FileName='furnace.vtu')

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# get layout
layout1 = GetLayout()

# show data in view
showMeshDisplay = Show(showMesh, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
showMeshDisplay.Representation = 'Surface With Edges'

# get the material library
materialLibrary1 = GetMaterialLibrary()

# split cell
layout1.SplitHorizontal(0, 0.5)

# set active view
SetActiveView(None)

# Create a new 'Render View'
renderView2 = CreateView('RenderView')
renderView2.AxesGrid = 'GridAxes3DActor'
renderView2.StereoType = 'Crystal Eyes'
renderView2.CameraFocalDisk = 1.0
renderView2.OSPRayMaterialLibrary = materialLibrary1

# assign view to a particular cell in the layout
AssignViewToLayout(view=renderView2, layout=layout1, hint=2)

# split cell
layout1.SplitVertical(1, 0.5)

# set active view
SetActiveView(None)

# Create a new 'Render View'
renderView3 = CreateView('RenderView')
renderView3.AxesGrid = 'GridAxes3DActor'
renderView3.StereoType = 'Crystal Eyes'
renderView3.CameraFocalDisk = 1.0
renderView3.OSPRayMaterialLibrary = materialLibrary1
# uncomment following to set a specific view size
# renderView3.ViewSize = [400, 400]

# assign view to a particular cell in the layout
AssignViewToLayout(view=renderView3, layout=layout1, hint=4)

# split cell
layout1.SplitVertical(2, 0.5)

# set active view
SetActiveView(None)

# Create a new 'Render View'
renderView4 = CreateView('RenderView')
renderView4.AxesGrid = 'GridAxes3DActor'
renderView4.StereoType = 'Crystal Eyes'
renderView4.CameraFocalDisk = 1.0
renderView4.OSPRayMaterialLibrary = materialLibrary1
# uncomment following to set a specific view size
# renderView4.ViewSize = [400, 400]

# assign view to a particular cell in the layout
AssignViewToLayout(view=renderView4, layout=layout1, hint=6)

# change representation type
showMeshDisplay.SetRepresentationType('Surface With Edges')

# show data in view
showMeshDisplay_1 = Show(showMesh, renderView2, 'UnstructuredGridRepresentation')

# change representation type
showMeshDisplay_1.SetRepresentationType('Surface With Edges')

# reset view to fit data
renderView2.ResetCamera()

# show data in view
showMeshDisplay_2 = Show(showMesh, renderView3, 'UnstructuredGridRepresentation')

# reset view to fit data
renderView3.ResetCamera()

# change representation type
showMeshDisplay_2.SetRepresentationType('Surface With Edges')

# show data in view
showMeshDisplay_3 = Show(showMesh, renderView4, 'UnstructuredGridRepresentation')

# change representation type
showMeshDisplay_3.SetRepresentationType('Surface With Edges')

# reset view to fit data
renderView4.ResetCamera()

# Properties modified on renderView123
renderView1.CameraParallelProjection = 1
renderView2.CameraParallelProjection = 1
renderView3.CameraParallelProjection = 1

# Properties modified on renderView.AxesGrid
#renderView1.AxesGrid.Visibility = 1
#renderView2.AxesGrid.Visibility = 1
#renderView3.AxesGrid.Visibility = 1
#renderView4.AxesGrid.Visibility = 1
showMeshDisplay.DataAxesGrid.GridAxesVisibility = 1
showMeshDisplay_1.DataAxesGrid.GridAxesVisibility = 1
showMeshDisplay_2.DataAxesGrid.GridAxesVisibility = 1
showMeshDisplay_3.DataAxesGrid.GridAxesVisibility = 1

# current camera placement for renderView1
renderView1.CameraPosition = [0, -cam123, 0]
renderView1.CameraFocalPoint = [0, 0, 0]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = cam123/4
renderView1.CameraParallelProjection = 1

# current camera placement for renderView2
renderView2.CameraPosition = [-cam123, 0, 0]
renderView2.CameraFocalPoint = [0, 0, 0]
renderView2.CameraViewUp = [0.0, 0.0, 1.0]
renderView2.CameraParallelScale = cam123/4
renderView2.CameraParallelProjection = 1

# current camera placement for renderView3
renderView3.CameraPosition = [0, 0, cam123]
renderView3.CameraFocalPoint = [0, 0, 0]
renderView3.CameraParallelScale = cam123/4
renderView3.CameraParallelProjection = 1

# current camera placement for renderView4
renderView4.CameraPosition = [-4.83/9*cam123, -4.31/9*cam123, 4.69/9*cam123]
renderView4.CameraFocalPoint = [0, 0, 0]
renderView4.CameraViewUp = [0.4/9*cam123, 0.4/9*cam123, 0.8/9*cam123]
renderView4.CameraParallelScale = cam123/4

# save screenshot
SaveScreenshot('showOFMesh.png', layout1, SaveAllViews=1, ImageResolution=[3840, 2160])
