# Summary

This demo-case considers an electromagnetically induced liquid metal flow in a rectangular furnace. MHD mixing is achieved by a rotating permanent magnet besides the furnace. The goal is to find magnet-furnace configurations that offer the best mixing conditions and efficiency. This can optimally be achieved by using high-performance computing (HPC). It offers much greater simulation speeds than individual workstations by allowing to decompose the problem and run it in parallel on many threads. This physical problem is mainly described within the framework of *OpenFOAM* and *Elmer*. Other software like *Salome*, *ParaView* serve a role in demo-case automation, building the model and result postprocessing.

<div align="center">
<img src="README_img/mhd-mixing-1.2Hz.png" width="500">
</div>

**This HPC demonstration was prepared as a part of the training and dissemination activities organized by the [EuroCC National HPC Competence Centre of Latvia](https://eurocc-latvia.lv/?lang=en).**

# Table of contents

[[_TOC_]]

# Problem background

The aim of the MHD mixing demo-case is to give a tool to metallurgical companies for accurately modeling liquid metal flows in their furnaces induced by various electromagnetic inductors, especially novel permanent magnet systems. Due to the scale of industrial processes and inherent complexity of MHD modeling, it is oftentimes not possible to accurately model such systems in a timely manner. Besides, cost efficiency of numerical software is often questionable. Because velocity measurements for validating numerical models at industrial scales are not possible, the model is validated against lab-scale experimental measurement data. The developed software package that comprises scalable open-source software will enable industrial companies to easily model their industrial furnaces using an HPC environment.

A cylindrical permanent magnet with defined magnetization and rotational frequency is placed besides the liquid metal furnace and it induces electric currents within the liquid. The currents, however, induce an electromagnetic force which generates flow and stirrs the liquid metal. The liquid is characterized by its density, kinematic viscosity, electric and thermal conductivities, thermal expansion, and specific heat capacity. No-slip condition for velocity is used for all furnace walls. Temperature disparity is initialized in the system to characterise the mixing process. The scheme of the model is shown below.

<img src="README_img/furnace.png" width="700">

Typical model parameter values (aluminium furnace) are shown in the tables below.

<table>
<tr><th>Physical parameter</th><th>Geometry parameter</th></tr>
<tr><td>

|Name| Value | Unit|
| :---                |  :----: |  :---  |
| Density, ρ          | 2400    | kg/m^3 |
| Kin. viscosity, ν        | 3e-6    | m^2/s  |
| El. conductivity, σ | 3e6     | S/m    |
| Th. conductivity, k | 187.7     | W/m K  |
| Sp. heat capacity, C | 1000     | J/kg K    |
| Th. expansion coeff., β | 4e-5     | 1/K    |
| Prandtl number, Pr | 0.05     | -    |
| Magnetization, B    | 1.42    | T      |
| Rot. frequency, f   | 1.2      | Hz  |

</td><td>

|Name| Value | Unit| 
| :---      |  :----: |  :---  |
| FURNACE_X | 2.5  | m  |
| FURNACE_Y | 2    | m  |
| FURNACE_Z | 0.6  | m  |
| MAGNET_RAD | 0.2 | m  |
| MAGNET_HEI | 0.8 | m  |
| MAGNET_ANGLE_Y | 0 | deg  |
| MAGNET_ANGLE_X | 90 | deg  |

</td></tr> </table>

Example of *OpenFOAM* geometry and mesh visualized by `showOFMesh.py` script.

<img src="README_img/showOFMesh.png" width="700">

Example of *Elmer* geometry and mesh visualized by `showMesh.py` script.

<img src="README_img/showMesh.png" width="700">

# Physics

This is a multiphysical problem. The rotating permanent magnet induces volume force in the furnace and generates flow. The flow velocity distribution is then taken into account in the force calculation. This helps answering questions like which orientation of the rotating permanent magnet mixes the metal more efficiently and how long does the mixing take.

The problem comprises three physics: hydrodynamics, electromagnetism, and heat transfer. Navier-Stokes equations govern the liquid metal motion, energy equation describes conductive and convective heat transfer, and magnetic induction equation derived from Maxwell's equations together with Ohm’s Law for conducting media describe the evolution of magnetic field and electric currents inside the furnace. Electromagnetic force is added to the Navier-Stokes equation as a source term. Velocity from Navier-Stokes equation is used for temperature convection.

# Software prerequisites

The software used in this demo-case package are all open-source. They have been modified and coupled together allowing to model electromagnetically induced liquid metal flow in the furnace and visualize the results for later analysis. The current package contains the following software (version):

* **Git** — To obtain this case directly from *GitLab* via the `clone` command.
* **Slurm** ([Slurm](https://slurm.schedmd.com/sbatch.html)) — Used for orchestrating the simulation array. Please consult the documentation/administrator of your computer cluster in case other workload manager is being used (e.g. *Torque*).
* **OpenFOAM** ([OpenFOAM-6.0](https://openfoam.org/))
* **Elmer FEM** ([Elmer-9.0](https://www.csc.fi/web/elmer))
* **EOF-Library** ([EOF-Library](https://eof-library.org/))
* **OpenMPI** ([OpenMPI-4.0.0](https://www.open-mpi.org/))
* **Gmsh** ([Gmsh-4.7.1](https://gmsh.info/))
* **ParaView** ([ParaView-5.9.1](https://www.paraview.org/))
* **Salome** ([Salome-9.6.0](https://salome-platform.org/))

DISCLAIMER! Compatibility issues may arise if other software versions are used.

# Obtaining the case

Download this case directly from GitLab by executing the command below. Alternatively, code can be downloaded as `.zip` or `.tar` archive and copied to the HPC cluster by other means.
```
git clone https://gitlab.com/eurocc-latvia/mhd-mixing-hpc-model.git
```

# Contents of this case

Navigate to the newly created folder using *change directory* command `cd` to access the demo-case run files. The folder can also be renamed.
```
cd mhd-mixing-hpc-model
```

Here is the overview of all files in this repository. The contents and use of these files are described in detail next.

* `0_orig` — Folder containing initial conditions for all the physical fields.
* `constant` — Folder containing material properties and other physical constants.
* `solvers` — Folder containing *OpenFOAM* customised physics solvers that can solve steady and transient MHD flows with heat transfer.
* `system` — Folder containing definitions of solvers, boundary conditions and other properties to obtain *OpenFOAM* solution.
* `README_img` — Folder containing images for this `readme` file.
* `makefile` — Modular bash script for removing old solutions, running the *OpenFOAM* simulation stages (mesh generation, obtaining solution, result file export, OpenFOAM post-processing script.
* `hpc` — Bash script for submitting the job to an HPC cluster where *Slurm* workload manager is installed.
* `case-harmonic.sif` — *Elmer* input file for a harmonic case where electromagnetism is defined. `case.sif` is then created duplicating the chosen study case.
* `case-transient.sif` — *Elmer* input file for a transient case where electromagnetism is defined. `case.sif` is then created duplicating the chosen study case.
* `furnace.geo` — *Gmsh* script for generating and meshing the furnace for OpenFOAM resolving viscous boundary layers.
* `geomeshtry.py` — *Python* script for generating geometry and mesh for *Elmer*. Calls `salomeToElmerAuto.py` and creates `meshElmer` folder where the mesh files reside.
* `salomeToElmerAuto.py` — *Python* function script for exporting *Elmer* mesh file that gets called by `geomeshtry.py`.
* `geomeshow.py` — *Python* script for generating a reduced mesh file `showMesh.unv` that gets converted to `showMesh.vtu` mesh file. It is then visualized by `showMesh.py` script showing the magnet and furnace system (see below).
* `showMesh.py` — *Python* script for generating `showMesh.png` image file for visualizing the magnet and furnace system. Reads in the `showMesh.vtu` mesh file.
* `showOFMesh.py` — *Python* script for generating `showOFMesh.png` image file for visualizing the liquid metal furnace system. Reads in the `showOFMesh.vtu` mesh file.
* `post.py` — Post processing script for generating `flowrate.png` and `flowrate.avi` files for visualizing flowrate through different cross sections of the furnace (see below).
* `post-temp.py` — Post processing script for generating `temperature.png` and `temperature.avi` files and a `.csv` data file for visualizing temperature distribution and its evolution in the furnace.
* `README.md` — This *readme* file.

# Running the case

The demo-case is set up from two main input files `hpc` and `makefile`. At the beginning of the `hpc` file are several lines that consider the resources allocated for this job. HPC environment variable `job-name` is chosen by the user. `partition`, `nodes` and `ntasks` specify which and how many HPC cluster nodes and processors will be used for calculations. `mail-type` and `mail-user` informs the user about certain demo-case run events like `NONE`, `BEGIN`, `END`, `FAIL`, `ALL` via e-mail.
```
#SBATCH --job-name=hpc-tasks		# Job name
#SBATCH --partition=power		# Partition name (choice of 'debug' and 'regular')
#SBATCH --nodes=1-1			# Nodes min-max, i.e. on one exclusively
#SBATCH --ntasks=24			# Number of tasks
#SBATCH --ntasks-per-core=1		# Override the default setting and use HyperThreading
#SBATCH --time=96:00:00			# Time limit, hrs:min:sec
#SBATCH --no-kill			# Don't kill the process (or at least try) if node switches off
#SBATCH --mail-type=END,FAIL		# Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=user@hpc.com 	# Where to send mail	
```

The next part in the `hpc` file is responsible for loading the necessary software and solvers. Three options are available for *OpenFOAM* `SOLVER_OF`: `mhdBuoyantBoussinesqSimple` or steady-state solver, `mhdBuoyantBoussinesqPimple` or transient solver and `mhdBuoyantBoussinesqPimpleAverage` or averaged EM force transient flow solver. The latter approximation works well for fast-rotating magnets (10 Hz <).
```
module purge
module load eof
source /software/openfoam/OpenFOAM-6.0/etc/bashrc
source /software/eof/EOF-Library/etc/bashrc
wmake solvers/$SOLVER_OF
sed -i "s|SOLVER_OF=[^\n]*|SOLVER_OF=$SOLVER_OF|" makefile
module load gmsh
module load paraview
module load salome
```

Last part in the `hpc` file contains calls to macros defined in `makefile`. Macros are named `clean`, `build`, `run`, `post` and `postfinal` for their respective roles.
```
make clean
make build
make run
make post
make postfinal
```

## Makefile definitions and macros

First, solver settings are defined. Mesh fineness is controlled by the `MESH` parameter. Its values range from 0-very coarse to 4-very fine. The default setting is 2 - moderate. User can choose either harmonic or transient case for *Elmer* under `CASEFILE` parameter. Flow turbulence solver `kOmegaSST` is set here by default. `YPLUS` is an indicator for the quality of mesh for turbulent flows. A value of 33.0 is set by default for the `kOmegaSST` model. Time steps maximum time for both *OpenFOAM* and *Elmer* are set through `TIMESTEP_OF`, `TIMESTEP_EL`, `MAX_TIME_OF` and `MAX_TIME_EL`. Output interval is controlled by `WRITE`.
```
MESH = 2 #0-Very coarse, 1-Coarse, 2-Moderate, 3-Fine, 4-Very fine
CASEFILE = case-harmonic#case-transient#
TURBULENCE = kOmegaSST
YPLUS = 33.0
MAX_TIME_OF = 60
MAX_TIME_EL = 1000000
TIMESTEP_OF = 0.01
TIMESTEP_EL = 0.01
WRITE = 100
```

Next, material properties and conditions are defined. `DENSITY`, `VISCOSITY`, `CONDUCTIVITY`, `TH_CONDUCTIVITY`, `TH_EXPANSION` and `HEAT_CAPACITY` depend on the material of choice. Viscosity, thermal conductivity, and heat capacity define the Prandtl number, here `PRANDTL`. `MAGNETISATION` in teslas and `FREQUENCY` in hertz define the permanent magnet properties and conditions.
```
DENSITY = 2400
VISCOSITY = 3e-6
CONDUCTIVITY = 3e6
TH_CONDUCTIVITY = 187.7
HEAT_CAPACITY = 1000
TH_EXPANSION = 4e-5
PRANDTL = 0.05
TEMPERATURE = 600
TAMP = 20
MAGNETISATION = 1.42
FREQUENCY = 1.2
```

Dimensions of the rectangular furnace and cylindrical magnet are set via the respective parameters `FURNACE_X`, `FURNACE_Y`, `FURNACE_Z`, `MAGNET_RAD` and `MAGNET_HEI`. The position of the magnet and its rotation is controlled by the parameters `MAGNET_X`, `MAGNET_Y`, `MAGNET_Z` and `MAGNET_ANGLE_Y`, `MAGNET_ANGLE_X`, respectively. By default, the magnet axis is parallel to *z*-axis. First rotation is around *y*-axis, and the second rotation is around the new *x*-axis.
```
FURNACE_X = 2.5
FURNACE_Y = 2
FURNACE_Z = 0.6
MAGNET_RAD = 0.2
MAGNET_HEI = 0.8
MAGNET_X = -1.75
MAGNET_Y = 0.6
MAGNET_Z = 0.0
MAGNET_ANGLE_Y = 0
MAGNET_ANGLE_X = 90
```

Macro `clean` clears old solutions and generated files by simply deleting them.
```
clean:
	@echo 'Cleaning up...'
	@. $(WM_PROJECT_DIR)/bin/tools/CleanFunctions;	cleanCase;	cleanSamples
	@rm -rf 0
	@rm -rf log
	@rm -rf results
	@rm -rf meshElmer
	@rm -f case.sif
	@rm -f *.avi
	@rm -f *.csv
	@rm -f *.gif
	@rm -f O2E*
	@rm -f *.png
	@rm -f *.unv
	@rm -f *.vtu
```

Macro `build` prepares the environment for the new case. The parameter values defined in the header are all updated using `sed` commands. Both *OpenFOAM* and *Elmer* meshes are built using `gmsh(ToFoam)`, `salome` and `ElmerGrid` commands. `setFields` initializes the temperature distribution which is set through a `sed` command altering `system/setFieldsDict` file. `pvpython showMesh.py` and `pvpython showOFMesh.py` commandlines visualize the newly built meshes in `showMesh.png` and `showOFMesh.png` images. Last two command lines carry out domain decomposition for *OpenFOAM* and *Elmer* meshes. `SLURM_NTASKS` variable is defined through `ntasks` in the `hpc` file.
```
build:
	@echo 'Building...'
	cp $(CASEFILE).sif case.sif
	[..]
	cp -r 0_orig 0
	gmsh -3 furnace.geo -o furnace.msh -format msh2
	gmshToFoam furnace.msh
	ElmerGrid 14 5 furnace.msh
	@rm -f furnace.msh
	changeDictionary
	setFields #initializes temperature distribution
	salome -t ./geomeshtry.py
	salome -t ./geomeshow.py
	pvpython showOFMesh.py
	pvpython showMesh.py
	decomposePar -force #decompose OpenFOAM
	ElmerGrid 2 2 meshElmer -metis $(shell expr $(SLURM_NTASKS) / 2) #decomposes Elmer
```

Macro `run` executes the program in parallel using `MPI` calls.
```
run:
	@echo 'Running...' #in case of hypert-hreading use option: --use-hwthread-cpus
	mpirun -n $(shell expr $(SLURM_NTASKS) / 2) $(SOLVER_OF) -parallel : -n $(shell expr $(SLURM_NTASKS) / 2) ElmerSolver_mpi case.sif #>> log &
```

Macro `post` is for post-processing the results. It reconstructs the decomposed *OpenFOAM* solution and generates captures and animations of the results. `post.py` prepares flowrate results, `post-temp.py` prepares temperature distribution results, including a `.csv` data file of the temperature deviation evolution that can be easily compared to other setups.
```
post:
	@echo 'Postprocessing...'
	reconstructPar -newTimes -noZero
	pvpython post.py
	pvpython post-temp.py
	$(SOLVER_OF) -postProcess -noZero -func yPlus > results
```

Macro `postfinal` clears the working directory from generated `processorN` folders and mesh interpolation files `O2E`.
```
postfinal:
	@rm -rf processor*
	@rm -f O2E*
```

## Running the case as a Slurm job

After the value input, run the case on HPC cluster, by submitting the batch job script as seen below.
```
sbatch hpc
```

# Examining the results

Some automated results are provided after execution of the postprocessing part `post` of the `makefile`. The user can view 3D rendered images and animations of flowrate (velocity arrows) and temperature (surface plot). Additionally, time histories of flowrate through different cross sections and temperature deviation are shown. The user can use the flowrate information to learn about mixing efficiency at various distances from the magnet and in different sectors of the furnace. Maximum velocity is also shown. Similarly, the user can use the temperature deviation history to determin the mixing time of the set magnet-furnace configuration. A `.csv` data file containing temperature deviation temporal data is generated for easy comparison between different case studies.

The results can be exmained manually by downloading the case files to a local repository. The main files/folders to download are the reconstructed time steps (`0.01`, `0.2`, `3`, `45`, etc.) and the `constant` folder for the *OpenFOAM* part and showMesh.vtu for visualizing the magnet location. Additionally, download `hpc` and `makefile` because they contain information about the simulation. Post-processing is carried out in *ParaView*. It has a standalone version and *Salome* built-in version. Run *ParaView* or alternatively run *Salome* and navigate to ParaVis module. Read the *OpenFOAM* output data (`File` -> `Open ParaView File`). Alternatively, use right-click in the pipeline browser to access filters. Navigate to your working directory and select any file (`hpc`, `makefile`,...). Select `OpenFOAM Reader` when prompted. The *OpenFOAM* written output files will be read into *ParaView*. All physical variable fields are enabled by default. Select only specific field variables in the `Cell Arrays` section to speed up data processing. Add `Temporal Interpolator` filter to the data (`Filters` -> `Temporal` -> `Temporal Interpolator`). Enter 1 second in the `Discrete Time Step Interval field`. Larger value allows for faster data processing. Add case details and time stamp (`Filters` -> `Temporal` -> `Annotate Time Filter`).

* For visualizing the flowrate, add calculator expression (`Filters` -> `Common` -> `Glyph`). Type `velocity` in the `Result Array Name` field and `mag(U)` in the expression field to create a second velocity magnitude array. Add vector representation (`Filters` -> `Data Analysis` -> `Integrate Variables`) and use `U` for orientation and scaling and `velocity` for coloring.  Add another calculator expression (`Filters` -> `Data Analysis` -> `Calculator`). Type `flowrate` in the `Result Array Name` field and `1000*abs(U_X)` in the expression field to calculate absolute velocity parallel to *x*-axis. Alternatively, type `U_Y` or `U_Z` for *y* or *z* directions depending on the relative magnet location. Add slice (`Filters` -> `Hyper Tree Grid` -> `Slice`) ans use `Origin` and `Normal` options for the placement and orientation of the slice. Integrate the absolute velocity over the slice to get flowrate (`Filters` -> `Data Analysis` -> `Integrate Variables`). Repeat these steps for other slices. Group the results (`Filters` -> `Hyper Tree Grid` -> `Group Datasets`) by selecting all created IntegrateVariables branches in the pipeline browser. Plot flowrate through different cross sections over time (`Filters` -> `Data Analysis` -> `Plot Data Over Time`). Select only flowrate in the Series Parameter section for faster data processing. Example of liquid metal flowrate through different cross sections of the furnace visualized by `post.py` script is shown below.

<div align="center">
<img src="README_img/flowrate.png" width="700" align="center">
</div>

* For visualizing the temperature field, add calculator expression (`Filters` -> `Calculator`) for temperature deviation to quantify the homogenization process of the melt. Type `deltaT` in the `Result Array Name` field and `(T-Tref)^2` in the expression field. Integrate the temperature deviation expression over the whole domain (`Filters` -> `Data Analysis` -> `Integrate Variables`). Calculate reverse expression `sigmaT` by taking a square root and dividing by the volume `sqrt(deltaT/volume)`. Plot integral temperature deviation over time (`Filters` -> `Data Analysis` -> `Plot Data Over Time`). Line plot data can be exported as `.csv` file for later formatting (`File` -> `Export...`). Example of temperature distribution and its evolution in the furnace visualized by `post-temp.py` script is shown below.

<div align="center">
<img src="README_img/temperature.png" width="700" align="center">
</div>

# Suggestions for altering the case

There are many other options that can be modified that have not been shown in this demo-case description. Users can generate their own geometries and meshes using *Salome*. They can change *OpenFOAM* solvers, settings like tolerances, output times, initial and boundary conditions by modifying the entries in the `constant` `system` and `0_orig` directories.
