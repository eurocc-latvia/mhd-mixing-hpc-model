from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

title = "kbm_90deg"
frequency = 1
temperature = 1000
furnaceDimX = 4.000
furnaceDimY = 1.246
furnaceDimZ = 0.570
vol = furnaceDimX*furnaceDimY*furnaceDimZ
maxXYZ = max(furnaceDimX, furnaceDimY, furnaceDimZ)
cam123 = maxXYZ*3.4

# create a new 'OpenFOAMReader'
hpc = OpenFOAMReader(FileName='./hpc')

# get animation scene
animationScene1 = GetAnimationScene()

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1508, 690]

# get layout
layout1 = GetLayout()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Properties modified on hpc
hpc.SkipZeroTime = 0

# create a new 'Temporal Interpolator'
temporalInterpolator1 = TemporalInterpolator(Input=hpc)

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# show data in view
temporalInterpolator1Display = Show(temporalInterpolator1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
temporalInterpolator1Display.Representation = 'Surface'

# Properties modified on temporalInterpolator1
temporalInterpolator1.DiscreteTimeStepInterval = 1.0

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# set scalar coloring
ColorBy(temporalInterpolator1Display, ('POINTS', 'T'))

# rescale color and/or opacity maps used to include current data range
temporalInterpolator1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
temporalInterpolator1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'T'
tLUT = GetColorTransferFunction('T')

# get opacity transfer function/opacity map for 'T'
tPWF = GetOpacityTransferFunction('T')

# Properties modified on hpc
hpc.CellArrays = ['T']

# create a new 'Calculator'
calculator1 = Calculator(Input=temporalInterpolator1)

# Properties modified on calculator1
calculator1.ResultArrayName = 'deltaT'

# Properties modified on calculator1
calculator1.Function = '(T-' + str(temperature) + ')^2'

# create a new 'Integrate Variables'
integrateVariables1 = IntegrateVariables(Input=calculator1)

# create a new 'Calculator'
calculator2 = Calculator(Input=integrateVariables1)

# Properties modified on calculator2
calculator2.ResultArrayName = 'sigmaT'

# Properties modified on calculator2
calculator2.Function = 'sqrt(deltaT/' + str(vol) + ')'

# create a new 'Plot Data Over Time'
plotDataOverTime1 = PlotDataOverTime(Input=calculator2)

# Create a new 'Quartile Chart View'
quartileChartView1 = CreateView('QuartileChartView')
# uncomment following to set a specific view size
# quartileChartView1.ViewSize = [400, 400]

# show data in view
plotDataOverTime1Display = Show(plotDataOverTime1, quartileChartView1, 'QuartileChartRepresentation')

# add view to a layout so it's visible in UI
AssignViewToLayout(view=quartileChartView1, layout=layout1, hint=0)

# Properties modified on plotDataOverTime1Display
plotDataOverTime1Display.SeriesVisibility = ['sigmaT (stats)']
plotDataOverTime1Display.SeriesColor = ['deltaT (stats)', '0', '0', '0', 'sigmaT (stats)', '0.889998', '0.100008', '0.110002', 'T (stats)', '0.220005', '0.489998', '0.719997', 'X (stats)', '0.300008', '0.689998', '0.289998', 'Y (stats)', '0.6', '0.310002', '0.639994', 'Z (stats)', '1', '0.500008', '0', 'N (stats)', '0.650004', '0.340002', '0.160006', 'Time (stats)', '0', '0', '0', 'vtkValidPointMask (stats)', '0.889998', '0.100008', '0.110002']
plotDataOverTime1Display.SeriesPlotCorner = ['N (stats)', '0', 'T (stats)', '0', 'Time (stats)', '0', 'X (stats)', '0', 'Y (stats)', '0', 'Z (stats)', '0', 'deltaT (stats)', '0', 'sigmaT (stats)', '0', 'vtkValidPointMask (stats)', '0']
plotDataOverTime1Display.SeriesLineStyle = ['N (stats)', '1', 'T (stats)', '1', 'Time (stats)', '1', 'X (stats)', '1', 'Y (stats)', '1', 'Z (stats)', '1', 'deltaT (stats)', '1', 'sigmaT (stats)', '1', 'vtkValidPointMask (stats)', '1']
plotDataOverTime1Display.SeriesLineThickness = ['N (stats)', '2', 'T (stats)', '2', 'Time (stats)', '2', 'X (stats)', '2', 'Y (stats)', '2', 'Z (stats)', '2', 'deltaT (stats)', '2', 'sigmaT (stats)', '2', 'vtkValidPointMask (stats)', '2']
plotDataOverTime1Display.SeriesMarkerStyle = ['N (stats)', '0', 'T (stats)', '0', 'Time (stats)', '0', 'X (stats)', '0', 'Y (stats)', '0', 'Z (stats)', '0', 'deltaT (stats)', '0', 'sigmaT (stats)', '0', 'vtkValidPointMask (stats)', '0']
plotDataOverTime1Display.SeriesMarkerSize = ['N (stats)', '4', 'T (stats)', '4', 'Time (stats)', '4', 'X (stats)', '4', 'Y (stats)', '4', 'Z (stats)', '4', 'deltaT (stats)', '4', 'sigmaT (stats)', '4', 'vtkValidPointMask (stats)', '4']

# Properties modified on quartileChartView1
quartileChartView1.LeftAxisTitle = 'Temperature deviation, K'

# Properties modified on quartileChartView1
quartileChartView1.BottomAxisTitle = 'Time, s'

# update the view to ensure updated data information
quartileChartView1.Update()

# show data in view
temporalInterpolator1Display = Show(temporalInterpolator1, renderView1, 'UnstructuredGridRepresentation')

# show color bar/color legend
temporalInterpolator1Display.SetScalarBarVisibility(renderView1, True)

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(Input=hpc)

# show data in view
annotateTimeFilter1Display_1 = Show(annotateTimeFilter1, renderView1, 'TextSourceRepresentation')

# Properties modified on annotateTimeFilter1Display_1
annotateTimeFilter1Display_1.WindowLocation = 'UpperCenter'

# Properties modified on annotateTimeFilter1Display_1
annotateTimeFilter1Display_1.FontSize = 20

# Properties modified on annotateTimeFilter1
annotateTimeFilter1.Format = 'MHD Mixing (' + str(title) + '), f=' + str(frequency) + 'Hz, Time: %.0f s'

# create a new 'XML Unstructured Grid Reader'
showMeshvtu = XMLUnstructuredGridReader(FileName=['./showMesh.vtu'])

# create a new 'Threshold'
threshold1 = Threshold(Input=showMeshvtu)

# show data in view
threshold1Display = Show(threshold1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold1Display.Representation = 'Wireframe'

# Properties modified on threshold1
threshold1.Scalars = ['POINTS', 'GeometryIds']

# Properties modified on threshold1
threshold1.ThresholdRange = [2, 2]

# Properties modified on threshold1Display
threshold1Display.Opacity = 0.5

# reset view to fit data
renderView1.ResetCamera()

# update the view to ensure updated data information
renderView1.Update()

# export view
ExportView('./' + str(title) + '_f' + str(frequency) + 'Hz.csv', view=quartileChartView1)

# current camera placement for renderView1
renderView1.CameraPosition = [-4.83/9*cam123, -4.31/9*cam123, 4.69/9*cam123]
renderView1.CameraFocalPoint = [0, 0, 0]
renderView1.CameraViewUp = [0.4/9*cam123, 0.4/9*cam123, 0.8/9*cam123]
renderView1.CameraParallelScale = cam123/4

# save screenshot
SaveScreenshot('./temperature.png', layout1, SaveAllViews=1,
    ImageResolution=[1920, 1080])

# save animation
SaveAnimation('./temperature.avi', layout1, SaveAllViews=1,
   ImageResolution=[1920, 1080],
   FrameRate=10,
   FrameWindow=[0,9999])

#import os
#import imageio
#
##png_dir = './animation'
#png_dir = ''
#images = []
#for file_name in sorted(os.listdir(png_dir)):
#    if file_name.endswith('.png'):
#        file_path = os.path.join(png_dir, file_name)
#        images.append(imageio.imread(file_path))
#imageio.mimsave('./post.gif', images)# trace generated using paraview version 5.8.0
