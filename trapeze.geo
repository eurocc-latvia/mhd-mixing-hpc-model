// Furnace characteristic dimensions
x0 = 3.15/2;//		z0|``|````````````````|``|
y0 = 2.94/2;//	z1 \ |				  | / z1
z0 = 0.4/2;//		-z0 \|________________|/
y2 = -2*z0*Sqrt(3);   // y2-y0 -y0		 y0	y1+y0
y1 = 0;
z1 = 0.8*z0;
z2 = z1;
//rotx = 1;
//roty = 0;
//rotz = 0;
//rotangle = -90/180*Pi;

x0=x0+1e-4;
y0=y0+1e-4;
z0=z0+1e-4;

globalMeshFineness = 2 ;
nx = Round(20*Sqrt(x0/Min(x0,Min(y0,z0))))*(1+globalMeshFineness); //mesh elements in x
ny = Round(20*Sqrt(y0/Min(x0,Min(y0,z0))))*(1+globalMeshFineness); //mesh elements in y
nz = Round(20*Sqrt(z0/Min(x0,Min(y0,z0))))*(1+globalMeshFineness); //mesh elements in z

Nx=nx+1;
Ny=ny+1;
Nz=nz+1;
frequency = 1;
magnet_rad = 0.2;
magnet_x = -2.11;
magnet_y = 0;
magnet_z = 0;
magnetgap = Sqrt(Floor(Abs(magnet_x)/x0)*(Abs(magnet_x)-x0)^2+Floor(Abs(magnet_y)/y0)*(Abs(magnet_y)-y0)^2+Floor(Abs(magnet_z)/z0)*(Abs(magnet_z)-z0)^2);
utau = 2*3.14*frequency*magnetgap/2.5;//where 2.5 is empirical coefficient
nu = 3.75e-7;//liquid metal viscosity
yplus = 200;//desired y+ for the chosen turbulence model
cl3 = 2.0*x0/Nx;//default element size at given points
//first = nu*yplus/utau;//first wall element size
first = Sqrt(2)*nu*yplus/(utau*Sqrt((2*Log10(utau*z0/nu)-6.5)^(-2.3)));//first wall element size
bump = 1.0/(2.0*cl3/first-1.0);//smallest-to-largest mesh element ratio
r=1.02;//neighbouring element growth ratio

//exterior (bounding box) of mesh
Point(1) = { -x0,	-y0,-z0, cl3};
Point(2) = { -x0,	 y0,-z0, cl3};
Point(3) = { -x0, y1+y0, z0, cl3};
Point(4) = { -x0,	 -y0, z0, cl3};
Point(5) = { -x0, y2-y0, z0, cl3};
Point(6) = { -x0, y2-y0, z1, cl3};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line(5) = {4, 5};
Line(6) = {5, 6};
Line(7) = {6, 1};
Line Loop(1) = {1, 2, 3, 4}; // Rectangle
Line Loop(2) = {-4, 5, 6, 7}; // Trapeze
Plane Surface(1) = {1}; // Outer unstructured region
Plane Surface(2) = {2}; // Outer unstructured region

//refinement towards the sides
ext1[] = Extrude {2*x0, 0, 0} { Surface{1}; };
ext2[] = Extrude {2*x0, 0, 0} { Surface{2}; };

Transfinite Line {14,15,19,23,41,45} = Nx Using Bump bump;//Progression r;//
Transfinite Line { 2, 4, 6} = Nz Using Bump bump;//Progression r;//
Transfinite Line { 10,12,33} = Nz Using Bump bump;//Progression r;//
Transfinite Line { -1, 3, -9, 11} = Ny Using Bump bump;//Ny Using Progression r;//
Transfinite Line { -5, 7, -32, 34} = Ny/2 Using Bump bump;//Ny Using Progression r;//

Transfinite Volume "*";
Recombine Volume "*";
Transfinite Surface "*";
Recombine Surface "*";

//Rotate {{rotx,roty,rotz}, {0,0,0}, rotangle} {Volume{ext1[1]};}

Physical Surface("front") = {1,2};
Physical Surface("back") = {ext1[0],ext2[0]};
Physical Surface("bot") = {ext1[2]};
Physical Surface("left") = {ext1[3]};
Physical Surface("top") = {ext1[4],ext2[3]};
Physical Surface("right") = {ext2[5],ext2[4]};
Physical Volume("fullbox") = {ext1[1], ext2[1]};//+
Show "*";
